import os
import MySQLdb
from lxml import etree, html


def file_get_contents(filename):
    with open(filename) as f:
        return f.read()


conn = MySQLdb.connect(host="localhost",
                       user="root",
                       passwd="asdasd",
                       db="kitten",
                       charset="utf8mb4")
x = conn.cursor()

counter = 1
for root, subfolders, files in os.walk('../csfd'):
    for file in files:
        file_path = '../csfd/' + file

        csfdId = file.split('.')[0].split("_")[1]

        csfd_html = file_get_contents(file_path)

        dom = html.fromstring(csfd_html)

        imdbId = ""
        for header in dom.find_class('imdb'):
            link = header.getparent()
            imdbId = link.get('href').split('/')[4]

        x.execute("""INSERT INTO raw_data(csfd, csfd_id, imdb_id) VALUES (%s,%s,%s)""", (csfd_html, csfdId, imdbId))

        if counter % 1000 == 0:
            print("commit at " + str(counter))
            conn.commit()

        counter += 1
