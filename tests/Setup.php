<?php

namespace Tests;


use App\Model\Entities\MotionPicture;
use App\Model\Entities\Movie;
use App\Model\Entities\Title;
use Kdyby\Doctrine\EntityManager;

class Setup
{
    /** @var EntityManager */
    private $em;

    /**
     * Setup constructor.
     * @param EntityManager $manager
     */
    function __construct(EntityManager $manager)
    {
        $this->em = $manager;
    }

    function createMovies(array $movies)
    {
        foreach ($movies as $imdb => $data) {
            $movie = new Movie();
            $movie->setImdbId($imdb);

            foreach ($data['titles'] as $langcode => $name) {
                $title = new Title($name, $langcode);
                $this->em->persist($title);

                $movie->addName($title);
            }

            $this->em->persist($movie);
        }
    }

    function createTvShows(array $shows)
    {
        foreach ($shows as $imdb => $data) {
            $movie = new Movie();
            $movie->setImdbId($imdb);

            foreach ($data['titles'] as $langcode => $name) {
                $title = new Title($name, $langcode);
                $this->em->persist($title);

                $movie->addName($title);
            }

            $this->em->persist($movie);
        }
    }
}