<?php

require __DIR__ . '/../vendor/autoload.php';

Tester\Environment::setup();

$configurator = new Nette\Configurator;
$configurator->setDebugMode(FALSE);
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/../app')
    ->register();

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.test.neon');

$container = $configurator->createContainer();

/** @var \Kdyby\Doctrine\EntityManager $em */
$em = $container->getByType(\Kdyby\Doctrine\EntityManager::class);

require 'Setup.php';
$setup = new \Tests\Setup($em);

$movies = \Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/data.neon'));
$setup->createMovies($movies['movies'], 'movie');
$setup->createMovies($movies['tv'], 'tv');

$em->flush();

return $container;
