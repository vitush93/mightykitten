#!/usr/bin/env bash

# flush temp and create sessions folder
echo "FLUSHING TEMP.."
rm -rf "temp/"*
mkdir "temp/sessions" &> /dev/null
chmod 777 -R "temp/"

echo "CREATING UPLOAD DIR.."
mkdir "www/upload"
chmod 777 -R "www/upload"

# import settings
echo "IMPORTING SETTINGS.."
php bin/import-settings.php

# create admin account
echo "CREATING ADMIN ACCOUNT.."
php bin/create-user.php admin@example.com admin asdasd admin