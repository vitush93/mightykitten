<?php

define('WWW_DIR', __DIR__);

$container = require __DIR__ . '/../app/bootstrap.php';

\Netpromotion\Profiler\Profiler::start('Application');

$container->getByType(Nette\Application\Application::class)->run();

\Netpromotion\Profiler\Profiler::finish('Application');
