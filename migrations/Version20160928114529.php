<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160928114529 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE motion_picture_rating (id INT AUTO_INCREMENT NOT NULL, motion_picture_id INT DEFAULT NULL, user_id INT DEFAULT NULL, rating INT NOT NULL, created DATETIME NOT NULL, ip VARCHAR(255) NOT NULL, INDEX IDX_B96A0E09BFDD6CEC (motion_picture_id), INDEX IDX_B96A0E09A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE motion_picture_rating ADD CONSTRAINT FK_B96A0E09BFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE motion_picture_rating ADD CONSTRAINT FK_B96A0E09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE motion_picture ADD up INT NOT NULL, ADD down INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE motion_picture_rating');
        $this->addSql('ALTER TABLE motion_picture DROP up, DROP down');
    }
}
