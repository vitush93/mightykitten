<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160928114732 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX rating_idx ON motion_picture_rating (rating)');
        $this->addSql('CREATE INDEX ip_idx ON motion_picture_rating (ip)');
        $this->addSql('CREATE UNIQUE INDEX rating_uniq ON motion_picture_rating (motion_picture_id, user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX rating_idx ON motion_picture_rating');
        $this->addSql('DROP INDEX ip_idx ON motion_picture_rating');
        $this->addSql('DROP INDEX rating_uniq ON motion_picture_rating');
    }
}
