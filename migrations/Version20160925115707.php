<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925115707 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE suggestion CHANGE from_id from_id INT DEFAULT NULL, CHANGE to_id to_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE suggestion ADD CONSTRAINT FK_DD80F31B78CED90B FOREIGN KEY (from_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE suggestion ADD CONSTRAINT FK_DD80F31B30354A65 FOREIGN KEY (to_id) REFERENCES motion_picture (id)');
        $this->addSql('CREATE INDEX IDX_DD80F31B78CED90B ON suggestion (from_id)');
        $this->addSql('CREATE INDEX IDX_DD80F31B30354A65 ON suggestion (to_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE suggestion DROP FOREIGN KEY FK_DD80F31B78CED90B');
        $this->addSql('ALTER TABLE suggestion DROP FOREIGN KEY FK_DD80F31B30354A65');
        $this->addSql('DROP INDEX IDX_DD80F31B78CED90B ON suggestion');
        $this->addSql('DROP INDEX IDX_DD80F31B30354A65 ON suggestion');
        $this->addSql('ALTER TABLE suggestion CHANGE from_id from_id INT NOT NULL, CHANGE to_id to_id INT NOT NULL');
    }
}
