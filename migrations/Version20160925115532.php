<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925115532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE actor (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, created DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE motion_picture (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, imdb_id VARCHAR(255) DEFAULT NULL, csfd_id VARCHAR(255) DEFAULT NULL, imdb_rating DOUBLE PRECISION DEFAULT NULL, imdb_votes INT DEFAULT NULL, created DATETIME NOT NULL, year VARCHAR(255) DEFAULT NULL, runtime VARCHAR(255) DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, poster VARCHAR(255) DEFAULT NULL, director VARCHAR(255) DEFAULT NULL, tomato_meter DOUBLE PRECISION DEFAULT NULL, tomato_rating DOUBLE PRECISION DEFAULT NULL, tomato_fresh INT DEFAULT NULL, tomato_rotten INT DEFAULT NULL, tomato_user_meter DOUBLE PRECISION DEFAULT NULL, tomato_user_rating DOUBLE PRECISION DEFAULT NULL, tomato_url VARCHAR(255) DEFAULT NULL, metascore VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_7C36EFBE53B538EB (imdb_id), UNIQUE INDEX UNIQ_7C36EFBE91B208F2 (csfd_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translations_picture (picture_id INT NOT NULL, translation_id INT NOT NULL, INDEX IDX_5544408AEE45BDBF (picture_id), UNIQUE INDEX UNIQ_5544408A9CAA2B25 (translation_id), PRIMARY KEY(picture_id, translation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE picture_genres (picture_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_390F5306EE45BDBF (picture_id), INDEX IDX_390F53064296D31F (genre_id), PRIMARY KEY(picture_id, genre_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE picture_actors (picture_id INT NOT NULL, actor_id INT NOT NULL, INDEX IDX_4ECF46F5EE45BDBF (picture_id), INDEX IDX_4ECF46F510DAF24A (actor_id), PRIMARY KEY(picture_id, actor_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre_translation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suggestion (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, added DATETIME NOT NULL, from_id INT NOT NULL, to_id INT NOT NULL, INDEX IDX_DD80F31BF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translations_genre (genre_id INT NOT NULL, translation_id INT NOT NULL, INDEX IDX_DC1F002D4296D31F (genre_id), UNIQUE INDEX UNIQ_DC1F002D9CAA2B25 (translation_id), PRIMARY KEY(genre_id, translation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting (`key` VARCHAR(255) NOT NULL, `value` VARCHAR(255) NOT NULL, `comment` VARCHAR(255) NOT NULL, PRIMARY KEY(`key`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE motion_picture_translation (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, plot LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE translations_picture ADD CONSTRAINT FK_5544408AEE45BDBF FOREIGN KEY (picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE translations_picture ADD CONSTRAINT FK_5544408A9CAA2B25 FOREIGN KEY (translation_id) REFERENCES motion_picture_translation (id)');
        $this->addSql('ALTER TABLE picture_genres ADD CONSTRAINT FK_390F5306EE45BDBF FOREIGN KEY (picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE picture_genres ADD CONSTRAINT FK_390F53064296D31F FOREIGN KEY (genre_id) REFERENCES genre (id)');
        $this->addSql('ALTER TABLE picture_actors ADD CONSTRAINT FK_4ECF46F5EE45BDBF FOREIGN KEY (picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE picture_actors ADD CONSTRAINT FK_4ECF46F510DAF24A FOREIGN KEY (actor_id) REFERENCES actor (id)');
        $this->addSql('ALTER TABLE suggestion ADD CONSTRAINT FK_DD80F31BF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE translations_genre ADD CONSTRAINT FK_DC1F002D4296D31F FOREIGN KEY (genre_id) REFERENCES genre (id)');
        $this->addSql('ALTER TABLE translations_genre ADD CONSTRAINT FK_DC1F002D9CAA2B25 FOREIGN KEY (translation_id) REFERENCES genre_translation (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE picture_actors DROP FOREIGN KEY FK_4ECF46F510DAF24A');
        $this->addSql('ALTER TABLE suggestion DROP FOREIGN KEY FK_DD80F31BF675F31B');
        $this->addSql('ALTER TABLE translations_picture DROP FOREIGN KEY FK_5544408AEE45BDBF');
        $this->addSql('ALTER TABLE picture_genres DROP FOREIGN KEY FK_390F5306EE45BDBF');
        $this->addSql('ALTER TABLE picture_actors DROP FOREIGN KEY FK_4ECF46F5EE45BDBF');
        $this->addSql('ALTER TABLE translations_genre DROP FOREIGN KEY FK_DC1F002D9CAA2B25');
        $this->addSql('ALTER TABLE picture_genres DROP FOREIGN KEY FK_390F53064296D31F');
        $this->addSql('ALTER TABLE translations_genre DROP FOREIGN KEY FK_DC1F002D4296D31F');
        $this->addSql('ALTER TABLE translations_picture DROP FOREIGN KEY FK_5544408A9CAA2B25');
        $this->addSql('DROP TABLE actor');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE motion_picture');
        $this->addSql('DROP TABLE translations_picture');
        $this->addSql('DROP TABLE picture_genres');
        $this->addSql('DROP TABLE picture_actors');
        $this->addSql('DROP TABLE genre_translation');
        $this->addSql('DROP TABLE suggestion');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE translations_genre');
        $this->addSql('DROP TABLE setting');
        $this->addSql('DROP TABLE motion_picture_translation');
    }
}
