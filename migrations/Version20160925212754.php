<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925212754 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE playlist (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_D782112DB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translations_playlist (playlist_id INT NOT NULL, translation_id INT NOT NULL, INDEX IDX_4EC8DF986BBD148 (playlist_id), UNIQUE INDEX UNIQ_4EC8DF989CAA2B25 (translation_id), PRIMARY KEY(playlist_id, translation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE playlists_motionpictures (playlist_id INT NOT NULL, motionpicture_id INT NOT NULL, INDEX IDX_D5043EAE6BBD148 (playlist_id), INDEX IDX_D5043EAEE5087593 (motionpicture_id), PRIMARY KEY(playlist_id, motionpicture_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE playlist_translation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE playlist ADD CONSTRAINT FK_D782112DB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE translations_playlist ADD CONSTRAINT FK_4EC8DF986BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE translations_playlist ADD CONSTRAINT FK_4EC8DF989CAA2B25 FOREIGN KEY (translation_id) REFERENCES playlist_translation (id)');
        $this->addSql('ALTER TABLE playlists_motionpictures ADD CONSTRAINT FK_D5043EAE6BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE playlists_motionpictures ADD CONSTRAINT FK_D5043EAEE5087593 FOREIGN KEY (motionpicture_id) REFERENCES motion_picture (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translations_playlist DROP FOREIGN KEY FK_4EC8DF986BBD148');
        $this->addSql('ALTER TABLE playlists_motionpictures DROP FOREIGN KEY FK_D5043EAE6BBD148');
        $this->addSql('ALTER TABLE translations_playlist DROP FOREIGN KEY FK_4EC8DF989CAA2B25');
        $this->addSql('DROP TABLE playlist');
        $this->addSql('DROP TABLE translations_playlist');
        $this->addSql('DROP TABLE playlists_motionpictures');
        $this->addSql('DROP TABLE playlist_translation');
    }
}
