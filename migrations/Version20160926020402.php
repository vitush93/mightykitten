<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160926020402 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE playlist_item (id INT AUTO_INCREMENT NOT NULL, playlist_id INT DEFAULT NULL, motion_picture_id INT DEFAULT NULL, added_by_id INT DEFAULT NULL, added DATETIME NOT NULL, up INT NOT NULL, down INT NOT NULL, status INT NOT NULL, INDEX IDX_BF02127C6BBD148 (playlist_id), UNIQUE INDEX UNIQ_BF02127CBFDD6CEC (motion_picture_id), INDEX IDX_BF02127C55B127A4 (added_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE playlist_item ADD CONSTRAINT FK_BF02127C6BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE playlist_item ADD CONSTRAINT FK_BF02127CBFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE playlist_item ADD CONSTRAINT FK_BF02127C55B127A4 FOREIGN KEY (added_by_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE playlists_motionpictures');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE playlists_motionpictures (playlist_id INT NOT NULL, motionpicture_id INT NOT NULL, INDEX IDX_D5043EAE6BBD148 (playlist_id), INDEX IDX_D5043EAEE5087593 (motionpicture_id), PRIMARY KEY(playlist_id, motionpicture_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE playlists_motionpictures ADD CONSTRAINT FK_D5043EAE6BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE playlists_motionpictures ADD CONSTRAINT FK_D5043EAEE5087593 FOREIGN KEY (motionpicture_id) REFERENCES motion_picture (id)');
        $this->addSql('DROP TABLE playlist_item');
    }
}
