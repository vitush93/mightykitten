<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160927171354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX name_idx ON actor (name)');
        $this->addSql('CREATE INDEX name_idx ON genre_translation (name)');
        $this->addSql('CREATE INDEX locale_idx ON genre_translation (locale)');
        $this->addSql('CREATE INDEX name_locale_idx ON genre_translation (name, locale)');
        $this->addSql('CREATE INDEX imdb_idx ON motion_picture (imdb_id)');
        $this->addSql('CREATE INDEX csfd_idx ON motion_picture (csfd_id)');
        $this->addSql('CREATE INDEX locale_idx ON motion_picture_translation (locale)');
        $this->addSql('CREATE INDEX name_idx ON playlist_translation (name)');
        $this->addSql('CREATE INDEX locale_idx ON playlist_translation (locale)');
        $this->addSql('CREATE INDEX name_locale_idx ON playlist_translation (name, locale)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX name_idx ON actor');
        $this->addSql('DROP INDEX name_idx ON genre_translation');
        $this->addSql('DROP INDEX locale_idx ON genre_translation');
        $this->addSql('DROP INDEX name_locale_idx ON genre_translation');
        $this->addSql('DROP INDEX imdb_idx ON motion_picture');
        $this->addSql('DROP INDEX csfd_idx ON motion_picture');
        $this->addSql('DROP INDEX locale_idx ON motion_picture_translation');
        $this->addSql('DROP INDEX name_idx ON playlist_translation');
        $this->addSql('DROP INDEX locale_idx ON playlist_translation');
        $this->addSql('DROP INDEX name_locale_idx ON playlist_translation');
    }
}
