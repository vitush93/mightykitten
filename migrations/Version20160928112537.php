<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160928112537 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE playlist_item_rating (id INT AUTO_INCREMENT NOT NULL, playlist_item_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, ip VARCHAR(255) NOT NULL, INDEX IDX_7BBDD5C2AD504544 (playlist_item_id), INDEX IDX_7BBDD5C2A76ED395 (user_id), UNIQUE INDEX rating_uniq (playlist_item_id, user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE playlist_item_rating ADD CONSTRAINT FK_7BBDD5C2AD504544 FOREIGN KEY (playlist_item_id) REFERENCES playlist_item (id)');
        $this->addSql('ALTER TABLE playlist_item_rating ADD CONSTRAINT FK_7BBDD5C2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE playlist_item_rating');
    }
}
