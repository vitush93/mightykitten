<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160929233248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE motion_pictures_tags (motion_picture_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_CA20D9AEBFDD6CEC (motion_picture_id), INDEX IDX_CA20D9AEBAD26311 (tag_id), PRIMARY KEY(motion_picture_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE playlists_tags (playlist_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_E1EB38336BBD148 (playlist_id), INDEX IDX_E1EB3833BAD26311 (tag_id), PRIMARY KEY(playlist_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created DATETIME NOT NULL, up INT NOT NULL, down INT NOT NULL, UNIQUE INDEX UNIQ_389B7835E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_rating (id INT AUTO_INCREMENT NOT NULL, tag_id INT DEFAULT NULL, user_id INT DEFAULT NULL, rating INT NOT NULL, created DATETIME NOT NULL, ip VARCHAR(255) NOT NULL, INDEX IDX_31C86F0EBAD26311 (tag_id), INDEX IDX_31C86F0EA76ED395 (user_id), INDEX rating_idx (rating), INDEX ip_idx (ip), UNIQUE INDEX rating_uniq (tag_id, user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE motion_pictures_tags ADD CONSTRAINT FK_CA20D9AEBFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE motion_pictures_tags ADD CONSTRAINT FK_CA20D9AEBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE playlists_tags ADD CONSTRAINT FK_E1EB38336BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE playlists_tags ADD CONSTRAINT FK_E1EB3833BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE tag_rating ADD CONSTRAINT FK_31C86F0EBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE tag_rating ADD CONSTRAINT FK_31C86F0EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE motion_pictures_tags DROP FOREIGN KEY FK_CA20D9AEBAD26311');
        $this->addSql('ALTER TABLE playlists_tags DROP FOREIGN KEY FK_E1EB3833BAD26311');
        $this->addSql('ALTER TABLE tag_rating DROP FOREIGN KEY FK_31C86F0EBAD26311');
        $this->addSql('DROP TABLE motion_pictures_tags');
        $this->addSql('DROP TABLE playlists_tags');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tag_rating');
    }
}
