<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160929211432 extends AbstractMigration
{
    /**
     *
     * WARNING: Required updated ocramius/proxy-manager otherwise:
     *      Compile Error: Cannot use $this as parameter
     */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE favorite (id INT AUTO_INCREMENT NOT NULL, motion_picture_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_68C58ED9BFDD6CEC (motion_picture_id), INDEX IDX_68C58ED9A76ED395 (user_id), UNIQUE INDEX user_favorite_uniq (user_id, motion_picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `ignore` (id INT AUTO_INCREMENT NOT NULL, motion_picture_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_D88D6E2BFDD6CEC (motion_picture_id), INDEX IDX_D88D6E2A76ED395 (user_id), UNIQUE INDEX user_ignore_uniq (user_id, motion_picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seen (id INT AUTO_INCREMENT NOT NULL, motion_picture_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_A4520A18BFDD6CEC (motion_picture_id), INDEX IDX_A4520A18A76ED395 (user_id), UNIQUE INDEX user_seen_uniq (user_id, motion_picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE to_watch (id INT AUTO_INCREMENT NOT NULL, motion_picture_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_5B377B16BFDD6CEC (motion_picture_id), INDEX IDX_5B377B16A76ED395 (user_id), UNIQUE INDEX user_to_watch_uniq (user_id, motion_picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_favorites (user_id INT NOT NULL, favorite_id INT NOT NULL, INDEX IDX_82141A14A76ED395 (user_id), UNIQUE INDEX UNIQ_82141A14AA17481D (favorite_id), PRIMARY KEY(user_id, favorite_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_seen (user_id INT NOT NULL, seen_id INT NOT NULL, INDEX IDX_6BDB4416A76ED395 (user_id), UNIQUE INDEX UNIQ_6BDB4416BFEEF0E (seen_id), PRIMARY KEY(user_id, seen_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_to_watch (user_id INT NOT NULL, to_watch_id INT NOT NULL, INDEX IDX_207650CCA76ED395 (user_id), UNIQUE INDEX UNIQ_207650CCA9C1F9B (to_watch_id), PRIMARY KEY(user_id, to_watch_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED9BFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE `ignore` ADD CONSTRAINT FK_D88D6E2BFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE `ignore` ADD CONSTRAINT FK_D88D6E2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE seen ADD CONSTRAINT FK_A4520A18BFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE seen ADD CONSTRAINT FK_A4520A18A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE to_watch ADD CONSTRAINT FK_5B377B16BFDD6CEC FOREIGN KEY (motion_picture_id) REFERENCES motion_picture (id)');
        $this->addSql('ALTER TABLE to_watch ADD CONSTRAINT FK_5B377B16A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_favorites ADD CONSTRAINT FK_82141A14A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_favorites ADD CONSTRAINT FK_82141A14AA17481D FOREIGN KEY (favorite_id) REFERENCES favorite (id)');
        $this->addSql('ALTER TABLE users_seen ADD CONSTRAINT FK_6BDB4416A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_seen ADD CONSTRAINT FK_6BDB4416BFEEF0E FOREIGN KEY (seen_id) REFERENCES seen (id)');
        $this->addSql('ALTER TABLE users_to_watch ADD CONSTRAINT FK_207650CCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_to_watch ADD CONSTRAINT FK_207650CCA9C1F9B FOREIGN KEY (to_watch_id) REFERENCES to_watch (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_favorites DROP FOREIGN KEY FK_82141A14AA17481D');
        $this->addSql('ALTER TABLE users_seen DROP FOREIGN KEY FK_6BDB4416BFEEF0E');
        $this->addSql('ALTER TABLE users_to_watch DROP FOREIGN KEY FK_207650CCA9C1F9B');
        $this->addSql('DROP TABLE favorite');
        $this->addSql('DROP TABLE `ignore`');
        $this->addSql('DROP TABLE seen');
        $this->addSql('DROP TABLE to_watch');
        $this->addSql('DROP TABLE users_favorites');
        $this->addSql('DROP TABLE users_seen');
        $this->addSql('DROP TABLE users_to_watch');
    }
}
