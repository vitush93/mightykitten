<?php

$container = require __DIR__ . '/../app/bootstrap.php';


/** @var \Kdyby\Doctrine\EntityManager $em */
$em = $container->getByType(\Kdyby\Doctrine\EntityManager::class);

$defaults = [
    'lang' => [
        'value' => 'cs',
        'comment' => 'default site language (lang code)'
    ]
];

$settings = $em->getRepository(\App\Model\Entities\Setting::class)->findAll();

foreach ($defaults as $key => $setting) {
    if ($em->find(\App\Model\Entities\Setting::class, $key)) {
        continue;
    }

    $s = new \App\Model\Entities\Setting($key);

    $s->set($setting['value']);
    $s->setComment($setting['comment']);

    $em->persist($s);
}
$em->flush();

