<?php

if (!isset($_SERVER['argv'][3])) {
    echo '
Add new user to database.

Usage: create-user.php <email> <username> <password> <role>
';
    exit(1);
}

list(, $email, $username, $password, $role) = $_SERVER['argv'];

$container = require __DIR__ . '/../app/bootstrap.php';

/** @var App\Model\UserManager $manager */
$manager = $container->getByType(App\Model\UserManager::class);

/** @var \Kdyby\Doctrine\EntityManager $em */
$em = $container->getByType(\Kdyby\Doctrine\EntityManager::class);

try {
    $manager->add($email, $username, $password, $role);
    $em->flush();
    echo "User added.\n";
} catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
    echo "Error: duplicate e-mail.\n";
    exit(1);
}
