#!/usr/bin/env bash

## Copy assets from bower_components/ to www/public folder

cp -r bower_components/bootstrap/dist/* www/public/front

cp  bower_components/jquery/dist/jquery.min.js www/public/front/js

cp -r bower_components/font-awesome/{css,fonts} www/public/front

cp bower_components/popper.js/dist/umd/popper.min.js www/public/front/js

cp -r app/assets/images www/public/front