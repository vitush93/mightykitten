#!/usr/bin/env bash

sudo rm -rf "temp/cache/"*
sudo chmod 777 -R "temp/"
sudo service php7.0-fpm reload