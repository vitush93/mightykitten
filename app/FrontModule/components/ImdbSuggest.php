<?php

namespace App\FrontModule\Components;


use Nette\Application\UI\Control;

class ImdbSuggest extends Control
{
    function handleSuggest($term)
    {
        $term = trim(strtolower($term));
        $search = str_replace(array(" ", "(", ")"), array("_", "", ""), $term); //format search term
        $firstchar = substr($search, 0, 1); //get first character
        $url = "http://sg.media-imdb.com/suggests/${firstchar}/${search}.json"; //format IMDb suggest URL
        $jsonp = @file_get_contents($url); //get JSONP data
        preg_match('/^imdb\$.*?\((.*?)\)$/ms', $jsonp, $matches); //convert JSONP to JSON
        $json = $matches[1];
        $arr = json_decode($json, true);

        $this->presenter->sendJson($arr['d']);
    }
}

interface IImdbSuggestFactory
{
    /** @return ImdbSuggest */
    function create();
}