<?php

namespace App\FrontModule\Components;


use Nette\Application\UI\Form;

class SearchBoxControl extends FormControl
{
    /**
     * @return Form
     */
    protected function createComponentSearchBox()
    {
        $this->form = new Form();

        $this->form->addText('term', 'Term')
            ->setRequired(true);
        $this->form->addSubmit('process', 'search');

        return $this->form;
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/searchbox.latte');

        $this->template->render();
    }
}

interface ISearchBoxControlFactory
{
    /** @return SearchBoxControl */
    function create();
}