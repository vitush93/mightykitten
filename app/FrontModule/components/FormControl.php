<?php

namespace App\FrontModule\Components;


use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\InvalidArgumentException;

abstract class FormControl extends Control
{
    /** @var Form */
    protected $form;

    /**
     * @return Form
     */
    function getForm()
    {
        return $this->form;
    }

    /**
     * @param $c
     */
    function addOnSuccess($c)
    {
        if (!is_callable($c)) throw new InvalidArgumentException();

        $this->form->onSuccess[] = $c;
    }
}