<?php

namespace App\FrontModule\Presenters;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use Model\TranslationHelper;

    /** @var Translator @inject */
    public $translator;

    /** @persistent */
    public $locale = 'en';

    /** @var EntityManager @inject */
    public $em;
}
