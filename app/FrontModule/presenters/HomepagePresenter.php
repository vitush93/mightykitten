<?php

namespace App\FrontModule\Presenters;


use App\FrontModule\Components\IImdbSuggestFactory;
use App\FrontModule\Components\ISearchBoxControlFactory;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class HomepagePresenter extends BasePresenter
{
    /** @var ISearchBoxControlFactory @inject */
    public $searchBoxFactory;

    /** @var IImdbSuggestFactory @inject */
    public $imdbSuggestFactory;

    /**
     * [SearchBox]
     *
     * @param Form $form
     * @param ArrayHash $values
     */
    function searchBoxSucceeded(Form $form, ArrayHash $values)
    {
        dump($values);
        die;
    }

    /**
     * @return \App\FrontModule\Components\ImdbSuggest
     */
    protected function createComponentImdbSuggest()
    {
        return $this->imdbSuggestFactory->create();
    }

    /**
     * @return \App\FrontModule\Components\SearchBoxControl
     */
    protected function createComponentSearchBox()
    {
        return $this->searchBoxFactory->create();
    }

}