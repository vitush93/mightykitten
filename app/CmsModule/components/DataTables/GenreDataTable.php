<?php

namespace App\CmsModule\Components\DataTables;


use App\CmsModule\Components\IDataTableControlFactory;
use App\Model\Entities\Genre;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class GenreDataTable extends Control
{
    /** @var EntityManager */
    private $em;

    /** @var IDataTableControlFactory */
    private $dataTableFactory;

    /**
     * GenreDataTable constructor.
     * @param EntityManager $entityManager
     * @param IDataTableControlFactory $controlFactory
     */
    function __construct(EntityManager $entityManager, IDataTableControlFactory $controlFactory)
    {
        parent::__construct();

        $this->em = $entityManager;
        $this->dataTableFactory = $controlFactory;
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    protected function createComponentTable()
    {
        $genres = $this->em->getRepository(Genre::class)->findAll();
        usort($genres, function ($a, $b){
            return strcmp($a->translate('en')->getName(), $b->translate('en')->getName());
        });

        $table = $this->dataTableFactory->create();
        $table->setItemsPerPage(25);
        $table->setData($genres, function ($row) {
            return $row->getId();
        });

        $table->addColumn('name', 'Name', function ($row) {
            return $row->translate('en')->getName() . ' / ' . $row->translate('cs')->getName();
        });

        $table->addAction('edit', 'pencil', false, function ($row) {
            return "<a href=\"" . $this->presenter->link('editGenre', $row->getId()) . "\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-pencil\"></i> Edit</a>";
        });
        $table->addAction('delete', 'trash', false, function ($row) {
            return "<a href=\"" . $this->presenter->link('removeGenre', $row->getId()) . "\" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i> Remove</a>";
        });

        return $table;
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/table.latte');

        $this->template->render();
    }
}

interface IGenreDataTableFactory
{
    /** @return GenreDataTable */
    function create();
}