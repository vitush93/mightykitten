<?php

namespace App\CmsModule\Components\DataTables;


use App\CmsModule\Components\DoctrineDataTableBuilder;
use App\CmsModule\Components\IDataTableControlFactory;
use App\Lib\InvalidOperationException;
use App\Model\Entities\User;
use App\Model\UserManager;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class UsersDataTable extends Control
{
    /** @var EntityManager */
    private $em;

    /** @var IDataTableControlFactory */
    private $dataTableFactory;

    /** @var UserManager */
    private $userManager;

    /** @var \Nette\Security\User */
    private $user;

    /**
     * UsersDataTable constructor.
     * @param EntityManager $entityManager
     * @param IDataTableControlFactory $dataTableControlFactory
     * @param UserManager $userManager
     * @param \Nette\Security\User $user
     */
    function __construct(EntityManager $entityManager, IDataTableControlFactory $dataTableControlFactory, UserManager $userManager, \Nette\Security\User $user)
    {
        parent::__construct();

        $this->em = $entityManager;
        $this->dataTableFactory = $dataTableControlFactory;
        $this->userManager = $userManager;
        $this->user = $user;
    }

    /**
     * Delete user by ID.
     *
     * @param $id
     */
    function deleteUser($id)
    {
        if ($id == $this->user->id) {
            $this->flashMessage("You can't remove yourself.", 'info');

            $this->redirect('this');
        }

        try {
            $this->userManager->remove($id);
            $this->em->flush();

            $this->flashMessage('User has been removed.', 'info');
        } catch (InvalidOperationException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }

        $this->redirect('this');
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    protected function createComponentTable()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        $table = $this->dataTableFactory->create();
        $table->disablePagination();

        $builder = new DoctrineDataTableBuilder($table);

        return $builder->setData($users, 'id')
            ->addFields(['email', 'username', 'role', 'created'])
            ->addAction('delete', 'trash', true)
            ->addActionHandler('delete', [$this, 'deleteUser'])
            ->getDataTable();
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/table.latte');


        $this->template->render();
    }
}

interface IUsersDataTableFactory
{
    /** @return UsersDataTable */
    function create();
}