<?php

namespace App\CmsModule\Components\DataTables;


use App\CmsModule\Components\IDataTableControlFactory;
use App\Model\Entities\MotionPicture;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class MoviesDataTable extends Control
{
    /** @var EntityManager */
    private $em;

    /** @var IDataTableControlFactory */
    private $dataTableFactory;

    /**
     * MoviesDataTable constructor.
     * @param EntityManager $entityManager
     * @param IDataTableControlFactory $tableControlFactory
     */
    function __construct(EntityManager $entityManager, IDataTableControlFactory $tableControlFactory)
    {
        parent::__construct();

        $this->em = $entityManager;
        $this->dataTableFactory = $tableControlFactory;
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    protected function createComponentTable()
    {
        $movies = $this->em->getRepository(MotionPicture::class)->createQueryBuilder('m')
            ->addSelect('t')
            ->join('m.translation', 't')
            ->where('t.locale = :locale')
            ->orderBy('t.title', 'ASC')
            ->setParameter('locale', 'en')
            ->getQuery()
            ->getResult();

        $table = $this->dataTableFactory->create();
        $table->setItemsPerPage(15);
        $table->setData($movies, function ($item) {
            return $item->getId();
        });

        $table->addColumn('image', 'Poster', function ($row) {
            $src = $row->getPoster() ? '/upload/' . $row->getPoster() : 'http://placehold.it/60x85';
            return '<img class="table-image" src="' . $src . '">';
        });
        $table->addColumn('title', 'Title', function ($row) {
            return $row->translate('en')->getTitle();
        });
        $table->addColumn('genre', 'Genre', function ($row) {
            return implode(', ', array_map(function ($item) {
                return $item->translate('en')->getName();
            }, $row->getGenres()->toArray()));
        });
        $table->addColumn('imdb', 'IMDB', function ($row) {
            if ($row->getImdbId()) {
                return '<a href="http://www.imdb.com/title/' . $row->getImdbId() . '" target="_blank">' . $row->getImdbId() . '</a>';
            } else {
                return 'N/A';
            }
        });
        $table->addColumn('csfd', 'ČSFD', function ($row) {
            if ($row->getCsfdId()) {
                return '<a href="http://www.csfd.cz/film/' . $row->getCsfdId() . '" target="_blank">' . $row->getCsfdId() . '</a>';
            } else {
                return 'N/A';
            }
        });
        $table->addAction('edit', 'pencil', false, function ($item) {
            return '<a href="'.$this->presenter->link('Movies:edit', $item->getId()).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>';
        });

        return $table;
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/table.latte');

        $this->template->render();
    }
}

interface IMoviesDataTableFactory {
    /** @return MoviesDataTable */
    function create();
}