<?php

namespace App\CmsModule\Components\DataTables;


use App\CmsModule\Components\IDataTableControlFactory;
use App\Model\Entities\Suggestion;
use App\Model\Suggestions\SuggestionManager;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class SuggestionsDataTable extends Control
{
    /** @var IDataTableControlFactory */
    private $dataTableFactory;

    /** @var SuggestionManager */
    private $suggestionManager;

    /** @var EntityManager */
    private $em;

    /**
     * SuggestionsDataTable constructor.
     * @param EntityManager $entityManager
     * @param SuggestionManager $suggestionManager
     * @param IDataTableControlFactory $dataTableControlFactory
     */
    function __construct(EntityManager $entityManager, SuggestionManager $suggestionManager, IDataTableControlFactory $dataTableControlFactory)
    {
        parent::__construct();

        $this->dataTableFactory = $dataTableControlFactory;
        $this->em = $entityManager;
        $this->suggestionManager = $suggestionManager;
    }

    /**
     * Delete suggestion by ID.
     *
     * @param $id
     */
    function deleteSuggestion($id)
    {
        $res = $this->suggestionManager->remove($id);
        if ($res) {
            $this->flashMessage('Suggestion removed.', 'info');

            $this->em->flush();
        } else {
            $this->flashMessage('Error occured while removing the suggestion.', 'danger');
        }

        $this->redirect('this');
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    function createComponentTable()
    {
        $suggestions = $this->em->getRepository(Suggestion::class)
            ->findBy(['source' => Suggestion::SOURCE_MANUAL], ['added' => 'DESC']);

        $table = $this->dataTableFactory->create();
        $table->disablePagination();

        $table->setData($suggestions, function ($row) {
            return $row->getId();
        });

        $table->addColumn('added', 'Added', function ($row) {
            return $row->getAdded()->format('j.n.Y H:i:s');
        });

        $table->addColumn('author', 'Added by', function ($row) {
            return $row->getAuthor()->getEmail();
        });

        $table->addColumn('from', 'From', function ($row) {
            return $row->getFrom()->translate('en')->getTitle();
        });

        $table->addColumn('to', 'To', function ($row) {
            return $row->getTo()->translate('en')->getTitle();
        });

        $table->addAction('delete', 'trash');
        $table->addActionHandler('delete', [$this, 'deleteSuggestion']);

        return $table;
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/table.latte');

        $this->template->render();
    }
}

interface ISuggestionsDataTableFactory
{
    /** @return SuggestionsDataTable */
    function create();
}