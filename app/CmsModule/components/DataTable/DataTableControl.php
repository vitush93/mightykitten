<?php

namespace App\CmsModule\Components;


use App\Lib\InvalidOperationException;
use Nette\Application\UI\Control;
use Nette\Utils\Paginator;

class DataTableControl extends Control
{
    private $data = [];
    private $columns = [];
    private $actions = [];
    private $pk = null;
    private $handlers = [];

    /** @persistent */
    public $page = 1;
    private $itemsPerPage = 10;
    private $paginator;
    private $disablePagination = false;

    /**
     * @param string $name Column name / unique identifier.
     * @param string $label Column display name.
     * @param callable $get how to retrieve column data from actual data row.
     * @return $this
     * @throws InvalidOperationException
     */
    function addColumn($name, $label, callable $get)
    {
        if (isset($this->columns[$name])) {
            throw new InvalidOperationException("Column with identifier $name already exists.");
        }

        $this->columns[$name] = [$label, $get];

        return $this;
    }

    /**
     * @param $id
     * @param $name
     * @throws InvalidOperationException
     */
    function handleAction($id, $name)
    {
        if (!isset($this->handlers[$name])) {
            throw new InvalidOperationException("Handler for action $name does not exists.");
        }

        call_user_func($this->handlers[$name], $id);
    }

    /**
     * @param string $actionName action unique identifier.
     * @param $callback
     */
    function addActionHandler($actionName, $callback)
    {
        $this->handlers[$actionName] = $callback;
    }

    private function renderActionNoPrompt($name, $icon)
    {
        return function ($row) use ($name, $icon) {
            return "<a href=\"" . $this->link('action', ['name' => $name, 'id' => ($this->pk)($row)]) . "\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-$icon\"></i> " . ucfirst($name) . "</a>";
        };
    }

    private function renderActionPrompt($name, $icon)
    {
        return function ($row) use ($name, $icon) {
            return "<a onclick=\"return confirm('Are you sure?');\" href=\"" . $this->link('action', ['name' => $name, 'id' => ($this->pk)($row)]) . "\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-$icon\"></i> " . ucfirst($name) . "</a>";
        };
    }

    /**
     * @param string $name action identifier.
     * @param string $icon
     * @param bool $prompt
     * @param callable $render
     * @return $this
     * @throws InvalidOperationException
     */
    function addAction($name, $icon, $prompt = false, callable $render = null)
    {
        if (isset($this->actions[$name])) {
            throw new InvalidOperationException("Action with identifier $name already exists.");
        }

        if (!$render) {
            if ($prompt) {
                $render = $this->renderActionPrompt($name, $icon);
            } else {
                $render = $this->renderActionNoPrompt($name, $icon);
            }
        }

        $this->actions[$name] = [$icon, $render];

        return $this;
    }

    /**
     * @param array $data
     * @param callable $getPrimaryKey how to retrieve row's primary key.
     * @return $this
     */
    function setData(array $data, callable $getPrimaryKey)
    {
        $this->data = $data;
        $this->pk = $getPrimaryKey;

        return $this;
    }

    function disablePagination()
    {
        $this->disablePagination = true;
    }

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    function render()
    {
        $this->template->setFile(__DIR__ . '/datatable.latte');

        $paginator = new Paginator();
        $paginator->setItemCount(count($this->data));
        $paginator->setItemsPerPage($this->itemsPerPage);
        $paginator->setPage($this->page);

        $this->template->paginator = $paginator;

        if ($this->disablePagination) {
            $this->template->data = $this->data;
        } else {
            $this->template->data = array_slice($this->data, $paginator->getOffset(), $this->itemsPerPage);
        }

        $this->template->columns = $this->columns;
        $this->template->actions = $this->actions;
        $this->template->disablePagination = $this->disablePagination;


        $this->template->render();
    }

}

interface IDataTableControlFactory
{
    /** @return DataTableControl */
    function create();
}