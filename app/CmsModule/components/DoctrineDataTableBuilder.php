<?php

namespace App\CmsModule\Components;


use Nette\SmartObject;

class DoctrineDataTableBuilder
{
    use SmartObject;

    /** @var DataTableControl */
    private $datatable;

    /** @var array */
    private $fields = [];

    /**
     * DataTableBuilder constructor.
     * @param DataTableControl $instance
     */
    function __construct(DataTableControl $instance)
    {
        $this->datatable = $instance;
    }

    private function getColumnData($row, $fieldName)
    {
        $getter = 'get' . ucfirst($fieldName);
        $var = $row->{$getter}();

        if (is_object($var)) {
            if ($var instanceof \DateTime) {
                return $var->format('j.n.Y H:i:s');
            } else {
                return (string)$var;
            }
        } else {
            return strval($var);
        }
    }

    /**
     * @param array $data
     * @param string $pk
     * @return $this
     */
    function setData(array $data, $pk)
    {
        $this->datatable->setData($data, function ($row) use ($pk) {
            $getter = 'get' . ucfirst($pk);

            return $row->{$getter}();
        });

        return $this;
    }

    /**
     * @param array $fields
     * @return $this
     */
    function addFields(array $fields)
    {
        $this->fields = array_values(
            array_merge($fields, $this->fields)
        );

        return $this;
    }

    /**
     * @param string $field
     * @return $this
     */
    function addField($field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * @param $name
     * @param $label
     * @param callable $get
     * @return $this
     */
    function addColumn($name, $label, callable $get)
    {
        $this->datatable->addColumn($name, $label, $get);

        return $this;
    }

    /**
     * @param $name
     * @param $icon
     * @param bool $prompt
     * @param callable|null $render
     * @return $this
     */
    function addAction($name, $icon, $prompt = false, callable $render = null)
    {
        $this->datatable->addAction($name, $icon, $prompt, $render);

        return $this;
    }

    /**
     * @param $actionName
     * @param $callback
     * @return $this
     */
    function addActionHandler($actionName, $callback)
    {
        $this->datatable->addActionHandler($actionName, $callback);

        return $this;
    }

    /**
     * @return DataTableControl
     */
    function getDataTable()
    {
        foreach ($this->fields as $field) {
            $this->datatable->addColumn($field, ucfirst($field), function ($row) use ($field) {
                return $this->getColumnData($row, $field);
            });
        }

        return $this->datatable;
    }

}