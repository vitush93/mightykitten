<?php

namespace App\CmsModule\Forms;


use App\Lib\BootstrapForm;
use App\Model\Entities\User;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Nette\SmartObject;

class AddUserFormFactory implements IFormFactory
{
    use SmartObject;

    /** @var Form */
    private $form;

    /** @var FormFactory */
    private $factory;

    /** @var UserManager */
    private $userManager;

    /**
     * AddUserFormFactory constructor.
     * @param FormFactory $formFactory
     * @param UserManager $userManager
     */
    function __construct(FormFactory $formFactory, UserManager $userManager)
    {
        $this->factory = $formFactory;
        $this->userManager = $userManager;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function onSuccess(Form $form, $values)
    {
        $this->userManager->add($values->email, $values->username, $values->password, $values->role);
    }

    /**
     * @return Form
     */
    function create()
    {
        $form = $this->factory->create();

        $form->addText('email', 'E-mail')
            ->addRule(Form::EMAIL)
            ->setRequired();
        $form->addText('password', 'Password')
            ->setRequired();
        $form->addText('username', 'Username')
            ->setRequired();
        $form->addSelect('role', 'Role', [
            User::ROLE_USER => 'User',
            User::ROLE_MOD => 'Moderator',
            User::ROLE_ADMIN => 'Admin'
        ])
            ->setPrompt('-select-')
            ->setRequired();
        $form->addSubmit('process', 'Add user');

        $form->onSuccess[] = [$this, 'onSuccess'];

        $this->form = BootstrapForm::makeBootstrap($form);

        return $this->form;
    }

}