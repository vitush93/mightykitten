<?php

namespace App\CmsModule\Forms;


use App\Lib\InvalidOperationException;
use App\Model\Entities\Suggestion;
use App\Model\Entities\User;
use App\Model\Suggestions\SuggestionManager;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class AddSuggestionFormFactory implements IFormFactory
{
    /** @var FormFactory */
    private $factory;

    /** @var \Nette\Security\User */
    private $user;

    /** @var SuggestionManager */
    private $suggestionManager;

    /** @var EntityManager */
    private $em;

    /**
     * AddSuggestionFormFactory constructor.
     * @param FormFactory $factory
     * @param \Nette\Security\User $user
     * @param EntityManager $entityManager
     * @param SuggestionManager $suggestionManager
     */
    function __construct(FormFactory $factory, \Nette\Security\User $user, EntityManager $entityManager, SuggestionManager $suggestionManager)
    {
        $this->factory = $factory;
        $this->user = $user;
        $this->suggestionManager = $suggestionManager;
        $this->em = $entityManager;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    function addSuggestionFormSucceeded(Form $form, ArrayHash $values)
    {
        /** @var User $user */
        $user = $this->em->find(User::class, $this->user->id);

        try {
            $suggestion = $this->suggestionManager->addByImdbID($user, $values->from_id, $values->to_id);
            $suggestion->setSource(Suggestion::SOURCE_MANUAL);
        } catch (InvalidOperationException $e) {
            $form->addError('This suggestions is already present in the database.');
        }
    }

    /**
     * @return Form
     */
    function create()
    {
        $form = $this->factory->create();

        $form->addText('from', 'From');
        $form->addHidden('from_id')
            ->setRequired('From field is required.');
        $form->addText('to', 'To');
        $form->addHidden('to_id')
            ->setRequired('To field is required.');
        $form->addSubmit('process', 'Add');

        $form->onSuccess[] = [$this, 'addSuggestionFormSucceeded'];

        return $form;
    }
}