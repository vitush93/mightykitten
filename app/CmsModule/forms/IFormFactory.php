<?php

namespace App\CmsModule\Forms;


use Nette\Application\UI\Form;

interface IFormFactory
{
    /**
     * @return Form
     */
    function create();
}