<?php

namespace App\CmsModule\Forms;


use App\Lib\BootstrapForm;
use App\Model\Entities\Setting;
use App\Model\SettingsManager;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\SmartObject;

class SettingsFormFactory implements IFormFactory
{
    use SmartObject;

    /** @var FormFactory */
    private $factory;

    /** @var EntityManager */
    private $em;

    /** @var SettingsManager */
    private $settingsManager;

    /**
     * SettingsFormFactory constructor.
     * @param FormFactory $formFactory
     * @param EntityManager $entityManager
     * @param SettingsManager $settingsManager
     */
    function __construct(FormFactory $formFactory, EntityManager $entityManager, SettingsManager $settingsManager)
    {
        $this->factory = $formFactory;
        $this->em = $entityManager;
        $this->settingsManager = $settingsManager;
    }

    /**
     * @param Form $form
     * @param $values
     */
    function onSuccess(Form $form, $values)
    {
        foreach ($values as $key => $value) {
            $this->settingsManager->set($key, $value);
        }
    }

    /**
     * @return Form
     */
    function create()
    {
        $form = $this->factory->create();

        /** @var Setting $s */
        foreach ($this->em->getRepository(Setting::class)->findAll() as $s) {
            $form->addText($s->key(), $s->key())
                ->setRequired()
                ->setOption('description', $s->getComment())
                ->setDefaultValue($s->get());
        }

        $form->addSubmit('process', 'Save');
        $form->onSuccess[] = [$this, 'onSuccess'];

        $form = BootstrapForm::makeBootstrap($form);

        return $form;
    }
}