<?php

namespace App\CmsModule\Forms;


use Nette\Application\UI\Form;
use Nette\SmartObject;

class FormFactory implements IFormFactory
{
    use SmartObject;

    /**
     * @return Form
     */
    function create()
    {
        return new Form();
    }
}