<?php

namespace App\CmsModule\Presenters;


use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

class SignPresenter extends BasePresenter
{
    protected function startup()
    {
        parent::startup();

        if ($this->user->isLoggedIn() && $this->action != 'logout') {
            $this->redirect('Homepage:default');
        }
    }

    protected function createComponentLoginForm()
    {
        $form = new Form();

        $form->addText('email', 'E-mail');
        $form->addPassword('password', 'Password');
        $form->addSubmit('process', 'Login');

        $form->onSuccess[] = function (Form $form, $values) {
            try {
                $this->user->setExpiration('14 days');
                $this->user->login($values->email, $values->password);

                $this->redirect('Homepage:default');
            } catch (AuthenticationException $e) {
                $this->flashMessage($e->getMessage());
            }
        };

        return $form;
    }

    function actionLogout()
    {
        $this->user->logout();

        $this->redirect('login');
    }
}