<?php

namespace App\CmsModule\Presenters;


use App\CmsModule\Components\DataTables\ISuggestionsDataTableFactory;
use App\CmsModule\Forms\AddSuggestionFormFactory;
use Nette\Application\UI\Form;

class SuggestPresenter extends BasePresenter
{
    /** @var ISuggestionsDataTableFactory @inject */
    public $suggestionDataTableFactory;

    /** @var AddSuggestionFormFactory @inject */
    public $addSuggestionFormFactory;

    /**
     * @return \App\CmsModule\Components\DataTables\SuggestionsDataTable
     */
    protected function createComponentManualEntries()
    {
        return $this->suggestionDataTableFactory->create();
    }

    /**
     * @return Form
     */
    protected function createComponentAddSuggestionForm()
    {
        $form = $this->addSuggestionFormFactory->create();
        $form->onSuccess[] = function (Form $form, $values) {
            $this->em->flush();

            $this->flashMessage('Suggestion added.', 'info');
            $this->redirect('this');
        };

        return $form;
    }
}