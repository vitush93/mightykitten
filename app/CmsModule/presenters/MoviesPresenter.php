<?php

namespace App\CmsModule\Presenters;


use App\CmsModule\Components\DataTables\IMoviesDataTableFactory;
use Nette\Application\UI\Form;

class MoviesPresenter extends BasePresenter
{
    /** @var IMoviesDataTableFactory @inject */
    public $moviesDataTableFactory;

    function actionEdit($id)
    {

    }

    protected function createComponentEditForm()
    {
        $form = new Form();



        return $form;
    }

    /**
     * @return \App\CmsModule\Components\DataTables\MoviesDataTable
     */
    protected function createComponentMoviesTable()
    {
        return $this->moviesDataTableFactory->create();
    }
}