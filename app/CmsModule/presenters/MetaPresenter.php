<?php

namespace App\CmsModule\Presenters;


use App\CmsModule\Components\DataTables\IGenreDataTableFactory;
use App\CmsModule\Components\IDataTableControlFactory;
use App\Lib\BootstrapForm;
use App\Model\Entities\Genre;
use App\Model\Entities\Tag;
use App\Model\MotionPicture\GenreManager;
use App\Model\MotionPicture\MetaInfo;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;

class MetaPresenter extends BasePresenter
{
    /** @var IDataTableControlFactory @inject */
    public $dataTableFactory;

    /** @var IGenreDataTableFactory @inject */
    public $genreDataTableFactory;

    /** @var GenreManager @inject */
    public $genreManager;

    /** @var MetaInfo @inject */
    public $metaInfo;

    function actionGenres()
    {
        $this['addGenreForm']->onSuccess[] = [$this, 'addGenre'];
    }

    function actionTags()
    {
        $this['addTagForm']->onSuccess[] = [$this, 'addTag'];
    }

    /**
     * @param $id
     */
    function actionRemoveTag($id)
    {
        $tag = $this->em->find(Tag::class, $id);
        if (!$tag) $this->error();

        try {
            $this->em->remove($tag);
            $this->em->flush();

            $this->flashMessage('Tag removed.', 'info');
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->flashMessage('Cannot remove tag: tag contains referenced data.', 'danger');
        }

        $this->redirect('tags');
    }

    /**
     * @param $id
     */
    function actionEditTag($id)
    {
        /** @var Tag $tag */
        $tag = $this->em->find(Tag::class, $id);
        if (!$tag) $this->error();

        /** @var Form $form */
        $form = $this['addTagForm'];
        $form->setDefaults([
            'name' => $tag->getName()
        ]);

        $form->onSuccess[] = [$this, 'editTag'];
        $this->setView('tags');
        $this->template->edit = true;
    }

    /**
     * @param Form $form
     * @param $values
     */
    function editTag(Form $form, $values)
    {
        /** @var Tag $tag */
        $tag = $this->em->find(Tag::class, $this->getParameter('id'));

        try {
            $tag->setName($values->name);

            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            $form->addError("Tag $values->name already exists.");
        }

        $this->redirect('tags');
    }

    /**
     * @param Form $form
     * @param $values
     */
    function addTag(Form $form, $values)
    {
        try {
            $this->metaInfo->addTag($values->name);

            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            $form->addError("Tag $values->name already exists.");
        }

        $this->redirect('tags');
    }

    /**
     * @param $id
     */
    function actionRemoveGenre($id)
    {
        /** @var Genre|null $genre */
        $genre = $this->em->find(Genre::class, $id);
        if (!$genre) $this->error();

        try {
            $this->em->remove($genre);
            $this->em->flush();

            $this->flashMessage('Genre removed.', 'info');
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->flashMessage('Cannot remove genre: contains items.', 'danger');
        }

        $this->redirect('genres');
    }

    /**
     * @param $id
     */
    function actionEditGenre($id)
    {
        /** @var Genre|null $genre */
        $genre = $this->em->find(Genre::class, $id);
        if (!$genre) $this->error();

        $form = $this['addGenreForm'];
        $form->setDefaults([
            'name_en' => $genre->translate('en')->getName(),
            'name_cs' => $genre->translate('cs')->getName()
        ]);

        $form->onSuccess[] = [$this, 'editGenre'];

        $this->setView('genres');
        $this->template->edit = true;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function editGenre(Form $form, $values)
    {
        /** @var Genre $genre */
        $genre = $this->em->find(Genre::class, $this->getParameter('id'));

        $genre->translate('en')->setName($values->name_en);
        $genre->translate('cs')->setName($values->name_cs);
        if ($genre->hasNewTranslations()) {
            $genre->persistNewTranslations($this->em);
        }

        $this->em->flush();
        $this->flashMessage('Changes saved.', 'info');
        $this->redirect('genres');
    }

    /**
     * @param Form $form
     * @param $values
     */
    function addGenre(Form $form, $values)
    {
        $genre = $this->genreManager->add($values->name_en, 'en');

        if ($values->name_cs) {
            $this->genreManager->addTranslation($genre, $values->name_cs, 'cs');
        }

        $this->em->flush();

        $this->flashMessage('Genre created.', 'success');
        $this->redirect('genres');
    }

    /**
     * @return Form
     */
    protected function createComponentAddGenreForm()
    {
        $form = new Form();

        $form->addText('name_en', 'Name (en)')
            ->setRequired();
        $form->addText('name_cs', 'Name (cs)')
            ->setOption('description', 'Optional field.');
        $form->addSubmit('process', 'Save');

        return BootstrapForm::makeBootstrap($form);
    }

    /**
     * @return \App\CmsModule\Components\DataTables\GenreDataTable
     */
    protected function createComponentGenreDataTable()
    {
        $table = $this->genreDataTableFactory->create();
        $table['table']->addActionHandler('edit', [$this, 'actionEditGenre']);

        return $table;
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    protected function createComponentTagDataTable()
    {
        $tags = $this->em->getRepository(Tag::class)->findBy([], ['name' => 'ASC']);

        $table = $this->dataTableFactory->create();
        $table->setData($tags, function ($row) {
            return $row->getId();
        });
        $table->setItemsPerPage(25);

        $table->addColumn('name', 'Name', function ($row) {
            return $row->getName();
        });
        $table->addAction('edit', 'pencil', false, function ($row) {
            return "<a href=\"" . $this->link('editTag', $row->getId()) . "\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-pencil\"></i> Edit</a>";
        });
        $table->addAction('delete', 'trash', false, function ($row) {
            return "<a href=\"" . $this->link('removeTag', $row->getId()) . "\" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i> Remove</a>";
        });

        return $table;
    }

    /**
     * @return Form
     */
    protected function createComponentAddTagForm()
    {
        $form = new Form();

        $form->addText('name', 'Name')->setRequired();
        $form->addSubmit('process', 'Save');

        return BootstrapForm::makeBootstrap($form);
    }
}