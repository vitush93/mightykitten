<?php

namespace App\CmsModule\Presenters;


use App\CmsModule\Components\IDataTableControlFactory;
use App\Lib\BootstrapForm;
use App\Lib\InvalidOperationException;
use App\Model\Entities\Playlist;
use App\Model\Entities\PlaylistItem;
use App\Model\Entities\User;
use App\Model\FileManager;
use App\Model\MotionPicture\MotionPictureManager;
use App\Model\Suggestions\PlaylistBuilder;
use App\Model\Suggestions\PlaylistManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;

class PlaylistPresenter extends BasePresenter
{
    /** @var PlaylistManager @inject */
    public $playlistManager;

    /** @var MotionPictureManager @inject */
    public $motionPictureManager;

    /** @var IDataTableControlFactory @inject */
    public $dataTableFactory;

    /**
     * @param $id
     */
    function actionItems($id)
    {
        /** @var Playlist $playlist */
        $playlist = $this->em->find(Playlist::class, $id);
        if (!$playlist) $this->error();

        $items = $playlist->getItems()->toArray();
        usort($items, function ($a, $b) {
            return $a->getOrder() - $b->getOrder();
        });

        $this->template->playlist = $playlist;
        $this->template->items = $items;
    }

    /**
     * @param $itemid
     */
    function handleRemoveItem($itemid)
    {
        $item = $this->em->find(PlaylistItem::class, $itemid);
        if (!$item) $this->error();

        $this->em->remove($item);
        $this->em->flush();

        $this->flashMessage('Playlist item removed.', 'info');
        $this->redirect('this');
    }

    /**
     * @param $id
     */
    function handleEditPlaylist($id)
    {
        /** @var Playlist $playlist */
        $playlist = $this->em->find(Playlist::class, $id);
        if (!$playlist) $this->error();

        $this['addPlaylistForm']->setDefaults([
            'name_en' => $playlist->translate('en')->getName(),
            'name_cs' => $playlist->translate('cs')->getName(),
            'description_en' => $playlist->translate('en')->getDescription(),
            'description_cs' => $playlist->translate('cs')->getDescription(),
            'type' => $playlist->getType(),
            'edit' => $playlist->getId()
        ]);
        $this['addPlaylistForm']['thumbnail']->setOption('description', 'Select a file to replace the old thumbnail.');

        $this->template->edit = true;
    }

    /**
     * @param $id
     */
    function handleDeletePlaylist($id)
    {
        $res = $this->playlistManager->remove($id);

        if ($res) {
            $this->flashMessage('Playlist removed.', 'info');
        } else {
            $this->flashMessage('Could not remove playlist.', 'danger');
        }

        $this->redirect('this');
    }

    /**
     * @return \App\CmsModule\Components\DataTableControl
     */
    protected function createComponentPlaylistTable()
    {
        $playlists = $this->em->getRepository(Playlist::class)
            ->findBy([], ['created' => 'DESC']);

        $table = $this->dataTableFactory->create();
        $table->setData($playlists, function ($row) {
            return $row->getId();
        });

        $table->addColumn('created', 'Created', function ($row) {
            return $row->getCreated()->format('j.n.Y H:i:s');
        });

        $table->addColumn('createdBy', 'Created by', function ($row) {
            return $row->getCreatedBy()->getEmail();
        });

        $table->addColumn('name', 'Name', function ($row) {
            return $row->translate('en')->getName() . ' / ' . $row->translate('cs')->getName();
        });

        $table->addColumn('type', 'Type', function ($row) {
            return ucfirst($row->getType());
        });

        $table->addColumn('items', 'Item count', function ($row) {
            return $row->getItems()->count();
        });

        $table->addAction('delete', 'trash', true);
        $table->addActionHandler('delete', [$this, 'handleDeletePlaylist']);

        $table->addAction('edit', 'pencil');
        $table->addActionHandler('edit', [$this, 'handleEditPlaylist']);

        $table->addAction('items', 'list', false, function ($row) {
            return '<a href="' . $this->link('Playlist:items', $row->getId()) . '" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i> Items</a>';
        });

        return $table;
    }

    /**
     * @param Form $form
     * @param $values
     * @return null|string
     */
    private function uploadPlaylistThumbnail(Form $form, $values)
    {
        $thumbnail = null;
        if ($values->thumbnail->error != UPLOAD_ERR_NO_FILE) {
            if (!$values->thumbnail->isOk()) {
                $form->addError('Error eccured during file upload.');

                return $thumbnail;
            } else {
                $thumbnail = FileManager::upload($values->thumbnail);
            }
        }

        return $thumbnail;
    }

    /**
     * @param Form $form
     * @param $values
     */
    private function savePlaylist(Form $form, $values)
    {
        $thumbnail = $this->uploadPlaylistThumbnail($form, $values);
        if ($form->hasErrors()) return;

        $user = $this->em->find(User::class, $this->user->id);

        /** @var PlaylistBuilder|Playlist $builder */
        $builder = $this->playlistManager->createBuilder($values->type)
            ->setName($values->name_en, 'en')
            ->setName($values->name_cs, 'cs')
            ->setDescription($values->description_en, 'en')
            ->setDescription($values->description_cs, 'cs');

        $builder->setThumbnail($thumbnail);
        $builder->setCreatedBy($user);

        $playlist = $builder->getPlaylist();
        if ($playlist->hasNewTranslations()) {
            $playlist->persistNewTranslations($this->em);
        }

        $this->em->persist($playlist);
        $this->em->flush();

        $this->flashMessage('Playlist created.', 'success');
        $this->redirect('this');
    }

    /**
     * @param Form $form
     * @param $values
     */
    private function editPlaylist(Form $form, $values)
    {
        /** @var Playlist $playlist */
        $playlist = $this->em->find(Playlist::class, $values->edit);
        if (!$playlist) $this->error();

        if ($values->thumbnail->isOk()) {
            $newThumbnail = FileManager::replace($playlist->getThumbnail(), $values->thumbnail);
        }

        $playlist->translate('en')->setName($values->name_en);
        $playlist->translate('cs')->setName($values->name_cs);
        $playlist->translate('en')->setDescription($values->description_en);
        $playlist->translate('cs')->setDescription($values->description_cs);
        $playlist->setType($values->type);
        if (isset($newThumbnail)) {
            $playlist->setThumbnail($newThumbnail);
        }

        $this->em->flush();

        $this->flashMessage('Playlist saved.', 'info');
        $this->redirect('this');
    }

    /**
     * @param Form $form
     * @param $values
     */
    function addPlaylistFormSucceeded(Form $form, $values)
    {
        if ($values->edit) {
            $this->editPlaylist($form, $values);
        } else {
            $this->savePlaylist($form, $values);
        }
    }

    /**
     * @param Form $form
     * @param $values
     */
    function addItemFormSucceeded(Form $form, $values)
    {
        try {
            $mp = $this->motionPictureManager->findOneOrCreate(['imdbId' => $values->imdb_id], $values->imdb_id);
        } catch (InvalidOperationException $e) {
            $this->flashMessage('Movie could not be found.', 'danger');
            $this->redirect('this');

            return;
        }

        /** @var Playlist $playlist */
        $playlist = $this->em->find(Playlist::class, $this->getParameter('id'));
        if (!$playlist) $this->error();

        $user = $this->em->find(User::class, $this->user->id);

        $item = new PlaylistItem();
        $item->setPlaylist($playlist);
        $item->setStatus(PlaylistItem::STATUS_APPROVED);
        $item->setMotionPicture($mp);
        $item->setAddedBy($user);

        try {
            $this->em->persist($item);
            $this->em->flush();

            $this->flashMessage('Playlist updated.', 'info');
        } catch (UniqueConstraintViolationException $e) {
            $this->flashMessage("{$values->imdb} is already in the playlist.", "danger");
        }

        $this->redirect('this');
    }

    /**
     * @return Form
     */
    protected function createComponentAddPlaylistForm()
    {
        $form = new Form();

        $form->addText('name_en', 'Name (en)')
            ->setRequired();
        $form->addText('name_cs', 'Name (cs)')
            ->setRequired();
        $form->addTextArea('description_en', 'Description (en)')
            ->setOption('description', 'Description is optional although both translations are required if any.');
        $form->addTextArea('description_cs', 'Description (cs)');
        $form->addUpload('thumbnail', 'Thumbnail')
            ->setOption('description', 'Thumbnail image is optional.')
            ->addCondition(Form::FILLED)
            ->addRule(Form::IMAGE);
        $form->addHidden('edit', false);
        $form->addSelect('type', 'Type', [
            Playlist::TYPE_MOVIE => 'Movie',
            Playlist::TYPE_SERIES => 'Series',
            Playlist::TYPE_MIXED => 'Mixed'
        ])->setPrompt('-select-')->setRequired();
        $form->addSubmit('process', 'Save');

        $form['description_en']
            ->addConditionOn($form['description_cs'], Form::FILLED)
            ->addRule(Form::FILLED);
        $form['description_cs']
            ->addConditionOn($form['description_en'], Form::FILLED)
            ->addRule(Form::FILLED);

        $form->onSuccess[] = [$this, 'addPlaylistFormSucceeded'];

        return BootstrapForm::makeBootstrap($form);
    }

    /**
     * @return Form
     */
    protected function createComponentAddItemForm()
    {
        $form = new Form();

        $form->addText('imdb', 'IMDB ID');
        $form->addHidden('imdb_id')
            ->setRequired();
        $form->addSubmit('process', 'Add');

        $form->onSuccess[] = [$this, 'addItemFormSucceeded'];

        return $form;
    }

    function orderFormSucceeded(Form $form)
    {
        $values = $form->getHttpData();

        foreach ($values['order'] as $itemId => $order) {

            /** @var PlaylistItem $item */
            $item = $this->em->find(PlaylistItem::class, $itemId);

            if (strlen($order) == 0) {
                $item->setOrder(0);
            } else {
                $item->setOrder(intval($order));
            }
        }

        $this->em->flush();

        $this->flashMessage('Order of items has been saved.', 'info');
        $this->redirect('this');
    }

    protected function createComponentOrderForm()
    {
        $form = new Form();

        $form->addSubmit('process', 'Save order');
        $form->onSuccess[] = [$this, 'orderFormSucceeded'];

        return BootstrapForm::makeBootstrap($form);
    }
}