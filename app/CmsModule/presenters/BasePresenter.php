<?php

namespace App\CmsModule\Presenters;


use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Presenter;

class BasePresenter extends Presenter
{
    /** @var EntityManager @inject */
    public $em;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn() && $this->presenter->action != 'login') {
            $this->redirect('Sign:login');
        }

        if(!$this->user->isAllowed($this->presenter->name)) {
            $this->flashMessage('Access denied.', 'danger');
            $this->redirect('Sign:logout');
        }
    }
}