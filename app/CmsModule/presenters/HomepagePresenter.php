<?php

namespace App\CmsModule\Presenters;


use App\CmsModule\Components\DataTables\IUsersDataTableFactory;
use App\CmsModule\Forms\AddUserFormFactory;
use App\CmsModule\Forms\SettingsFormFactory;
use App\Model\Entities\Setting;
use App\Model\Entities\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;

class HomepagePresenter extends BasePresenter
{
    /** @var AddUserFormFactory @inject */
    public $addUserFormFactory;

    /** @var SettingsFormFactory @inject */
    public $settingsFormFactory;

    /** @var EntityManager @inject */
    public $em;

    /** @var IUsersDataTableFactory @inject */
    public $usersDataTableFactory;

    function renderUsers()
    {
        $this->template->users = $this->em->getRepository(User::class)->findAll();
    }

    function renderSettings()
    {
        $this->template->settings = $this->em->getRepository(Setting::class)->findAll();
    }

    /**
     * @return \App\CmsModule\Components\DataTables\UsersDataTable
     */
    protected function createComponentUsersDataTable()
    {
        return $this->usersDataTableFactory->create();
    }

    /**
     * @return Form
     */
    protected function createComponentSettingsForm()
    {
        $form = $this->settingsFormFactory->create();
        $form->onSuccess[] = function (Form $form, $values) {
            $this->em->flush();

            $this->flashMessage('Changes saved.', 'info');
        };

        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentAddUserForm()
    {
        $form = $this->addUserFormFactory->create();
        $form->onSuccess[] = function (Form $form, $values) {
            try {
                $this->em->flush();

                $this->flashMessage('User has been created successfully.', 'success');
                $this->redirect('this');
            } catch (UniqueConstraintViolationException $e) {
                $this->flashMessage("User with e-mail {$values->email} already exists.", 'danger');
            }
        };

        return $form;
    }
}