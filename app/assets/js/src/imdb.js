export default class ImdbSuggest {
    constructor(selector) {
        this.el = selector;
    }

    static url(term) {
        return '/?imdbSuggest-term=' + term + '&do=imdbSuggest-suggest';
    }

    init() {
        $(document).on('keyup', this.el, function () {
            console.log($(this).val());
        });
    }
}