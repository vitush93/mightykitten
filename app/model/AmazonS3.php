<?php

namespace App\Model;


use Aws\S3\S3Client;

class AmazonS3
{
    /**
     * @var S3Client
     */
    private $client;

    /**
     * AmazonS3 constructor.
     * @param S3ClientFactory $factory
     */
    function __construct(S3ClientFactory $factory)
    {
        $this->client = $factory->create();
    }

    /**
     * @param $bucket
     * @return string
     */
    private function getActualBucketName($bucket)
    {
        return 'mightykitten-' . $bucket;
    }

    /**
     * @param string $bucket unique name of the bucket located on the S3
     * @param string $localPath absolute local path file to be used with fopen(..)
     * @param string $remotePath path without starting slash eg 'path/to/file.ext'
     */
    function uploadFile($bucket, $localPath, $remotePath)
    {
        $bucket = $this->getActualBucketName($bucket);

        $this->client->putObject([
            'Bucket' => $bucket,
            'Key' => $remotePath,
            'Body' => fopen($localPath, 'r+'),
        ]);
    }

    /**
     * @param $bucket
     * @param $localPath
     * @param $remotePath
     * @param $oldRemotePath
     */
    function replaceFile($bucket, $localPath, $remotePath, $oldRemotePath)
    {
        $this->uploadFile($bucket, $localPath, $remotePath);

        $this->removeFile($bucket, $oldRemotePath);
    }

    /**
     * @param $bucket
     * @param $remotePath
     */
    function removeFile($bucket, $remotePath)
    {
        $this->client->deleteObject([
            'Bucket' => $bucket,
            'Key' => $remotePath
        ]);
    }
}