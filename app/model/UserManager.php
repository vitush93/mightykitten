<?php

namespace App\Model;


use App\Lib\InvalidOperationException;
use App\Model\Entities\User;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Nette\SmartObject;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManager implements IAuthenticator
{
    use SmartObject;

    /** @var EntityManager */
    private $em;

    /** @var EventManager */
    private $evm;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * UserManager constructor.
     * @param EntityManager $entityManager
     * @param EventManager $eventManager
     * @param ValidatorInterface $validator
     */
    function __construct(EntityManager $entityManager, EventManager $eventManager, ValidatorInterface $validator)
    {
        $this->em = $entityManager;
        $this->evm = $eventManager;
        $this->validator = $validator;
    }

    /**
     * @param $email
     * @param $username
     * @param $password
     * @param string $role
     */
    function add($email, $username, $password, $role = 'user')
    {
        $user = new User($email, $username, $password);

        $user->setRole($role);

        $this->em->persist($user);

        $this->evm->dispatchEvent(Events::USER_ADD, new EventArgsList([$user]));
    }

    /**
     * @param User|int $user
     * @throws InvalidOperationException
     */
    function remove($user)
    {
        if ($user instanceof User) {
            $this->em->remove($user);
        } else {
            $user = $this->em->find(User::class, $user);

            if ($user) {
                $this->em->remove($user);
            } else {
                throw new InvalidOperationException("Cannot remove: user not found.");
            }
        }

        $this->evm->dispatchEvent(Events::USER_REMOVE, new EventArgsList([$user]));
    }

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     * @param array $credentials
     * @return IIdentity
     * @throws AuthenticationException
     */
    function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;

        /** @var User|null $user */
        $user = $this->em->getRepository(User::class)->findOneBy([
            'email' => $email
        ]);

        if (!$user) {
            throw new AuthenticationException('E-mail is incorrect.', self::IDENTITY_NOT_FOUND);
        }

        if (!Passwords::verify($password, $user->getPassword())) {
            throw new AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        } elseif (Passwords::needsRehash($user->getPassword())) {
            $user->setPassword($password);
        }

        return new Identity($user->getId(), $user->getRole(), [
            'email' => $user->getEmail(),
            'username' => $user->getUsername()
        ]);
    }
}