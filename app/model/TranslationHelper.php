<?php

namespace App\Model;


trait TranslationHelper
{
    function _(...$params)
    {
        return call_user_func_array([$this->translator, 'translate'], $params);
    }
}