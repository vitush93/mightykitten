<?php

namespace App\Model;


use GuzzleHttp\Client;
use Nette\Http\FileUpload;
use Nette\SmartObject;

class FileManager
{
    use SmartObject;

    const UPLOAD_DIR = WWW_DIR . '/upload';

    /**
     * @param FileUpload $file
     * @return string
     */
    public static function upload(FileUpload $file)
    {
        $filename = $file->getSanitizedName();

        $uploadDir = self::getUploadDir();

        $pathinfo = pathinfo($filename);
        $filename = $pathinfo['filename'] . '_' . uniqid(mt_rand(), true) . '.' . $pathinfo['extension'];

        $file->move(self::UPLOAD_DIR . '/' . $uploadDir . '/' . $filename);

        return $uploadDir . '/' . $filename;
    }

    /**
     * @param $filename
     */
    public static function delete($filename)
    {
        $path = self::UPLOAD_DIR . '/' . $filename;

        if (file_exists($path) && is_file($path)) {
            unlink($path);
        }
    }

    /**
     * @param $old_filename
     * @param FileUpload $new_file
     * @return string
     */
    public static function replace($old_filename, FileUpload $new_file)
    {
        $new = self::upload($new_file);
        self::delete($old_filename);

        return $new;
    }

    /**
     * @return string
     */
    private static function getUploadDir()
    {
        $uploadDir = date('Y') . '/' . date('m');

        $actualUploadDir = self::UPLOAD_DIR . '/' . $uploadDir;
        if (!file_exists($actualUploadDir)) {
            mkdir($actualUploadDir, 0777, true);
        }

        return $uploadDir;
    }

    /**
     * @param $url
     * @return string
     */
    private function getSaveUrl($url)
    {
        $uploadDir = self::getUploadDir();

        $extension = explode('.', $url);
        $extension = $extension[count($extension) - 1];

        return $uploadDir . '/' . uniqid(mt_rand(), true) . '.' . $extension;
    }

    /**
     * @param $url
     * @return bool
     */
    function fetch($url)
    {
        $client = new Client();
        $res = $client->get($url);
        $image = $res->getBody()->getContents();

        $localFilePath = $this->getSaveUrl($url);
        $actualLocalFilePath = self::UPLOAD_DIR . '/' . $localFilePath;

        $fp = fopen($actualLocalFilePath, 'x');
        fwrite($fp, $image);
        fclose($fp);

        if (file_exists($actualLocalFilePath)) {
            return $localFilePath;
        } else {
            return null;
        }
    }
}