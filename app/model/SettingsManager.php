<?php

namespace App\Model;


use App\Lib\InvalidArgumentException;
use App\Model\Entities\Setting;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class SettingsManager
{
    use SmartObject;

    /** @var EntityManager */
    private $em;

    /**
     * SettingsManager constructor.
     * @param EntityManager $entityManager
     */
    function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return \Kdyby\Doctrine\EntityRepository
     */
    private function repository()
    {
        return $this->em->getRepository(Setting::class);
    }

    /**
     * @param $key
     * @return Setting
     * @throws InvalidArgumentException
     */
    private function _get($key)
    {
        /** @var Setting $setting */
        $setting = $this->repository()->findOneBy(['key' => $key]);
        if (!$setting) {
            throw new InvalidArgumentException("Setting with key '$key' does not exists.");
        }

        return $setting;
    }

    /**
     * @param $key
     * @return int
     * @throws InvalidArgumentException
     */
    function get($key)
    {
        $setting = $this->_get($key);

        return $setting->get();
    }

    /**
     * @param $key
     * @param $value
     */
    function set($key, $value)
    {
        $setting = $this->_get($key);

        $setting->set($value);
    }


}