<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class ToWatch
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_to_watch_uniq", columns={"user_id", "motion_picture_id"})})
 */
class ToWatch
{
    use SmartObject;
    use UserList;
}