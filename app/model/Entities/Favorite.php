<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Favorite
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_favorite_uniq", columns={"user_id", "motion_picture_id"})})
 */
class Favorite
{
    use SmartObject;
    use UserList;
}