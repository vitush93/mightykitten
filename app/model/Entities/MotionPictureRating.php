<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use App\Lib\InvalidArgumentException;
use Nette\SmartObject;

/**
 * Class MotionPictureRating
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(
 *     indexes={
 *          @ORM\Index(name="rating_idx", columns={"rating"}),
 *          @ORM\Index(name="ip_idx", columns={"ip"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="rating_uniq", columns={"motion_picture_id", "user_id"})
 *     })
 */
class MotionPictureRating
{
    use SmartObject;

    const RATING_UP = 1,
        RATING_DOWN = 2;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var MotionPicture
     * @ORM\ManyToOne(targetEntity="MotionPicture")
     */
    private $motionPicture;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="motionPictureRatings")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * MotionPictureRating constructor.
     * @param $rating
     */
    function __construct($rating)
    {
        $this->setRating($rating);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    function setUp()
    {
        $this->rating = self::RATING_UP;
    }

    function setDown()
    {
        $this->rating = self::RATING_DOWN;
    }

    /**
     * @return MotionPicture
     */
    public function getMotionPicture()
    {
        return $this->motionPicture;
    }

    /**
     * @param MotionPicture $motionPicture
     */
    public function setMotionPicture($motionPicture)
    {
        $this->motionPicture = $motionPicture;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @throws InvalidArgumentException
     */
    public function setRating($rating)
    {
        if (!in_array($rating, [self::RATING_DOWN, self::RATING_UP])) {
            throw new InvalidArgumentException("Rating $rating not recognozed.");
        }

        $this->rating = $rating;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

}