<?php

namespace App\Model\Entities;


use App\Lib\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class TagRating
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(
 *     indexes={
 *          @ORM\Index(name="rating_idx", columns={"rating"}),
 *          @ORM\Index(name="ip_idx", columns={"ip"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="rating_uniq", columns={"tag_id", "user_id"})
 *     })
 */
class TagRating
{
    use SmartObject;

    const RATING_UP = 1,
        RATING_DOWN = 2;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var Tag
     * @ORM\ManyToOne(targetEntity="Tag")
     */
    private $tag;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * TagRating constructor.
     * @param $rating
     */
    function __construct($rating)
    {
        $this->created = new \DateTime();

        $this->setRating($rating);
    }

    /**
     * @return Tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param Tag $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @throws InvalidArgumentException
     */
    public function setRating($rating)
    {
        if (!in_array($rating, [self::RATING_DOWN, self::RATING_UP])) {
            throw new InvalidArgumentException("Rating $rating is not a valid rating value.");
        }

        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}