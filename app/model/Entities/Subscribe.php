<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Subscribe
 * @package App\Model\Entities
 * @ORM\Entity()
 */
class Subscribe
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * Subscribe constructor.
     * @param $email
     */
    function __construct($email)
    {
        $this->setEmail($email);

        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}