<?php

namespace App\Model\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class MotionPictureGenre
 * @package App\Model\Entities
 * @ORM\Entity()
 */
class Genre
{
    use SmartObject;
    use Translatable;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="GenreTranslation")
     * @ORM\JoinTable(name="translations_genre",
     *                joinColumns={@ORM\JoinColumn(name="genre_id", referencedColumnName="id")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="translation_id", referencedColumnName="id", unique=true)})
     */
    private $translation;

    function __construct()
    {
        $this->translation = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}