<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class PlaylistTranslation
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *          @ORM\Index(name="name_idx", columns={"name"}),
 *          @ORM\Index(name="locale_idx", columns={"locale"}),
 *          @ORM\Index(name="name_locale_idx", columns={"name", "locale"})
 *     })
 */
class PlaylistTranslation
{
    use SmartObject;
    use Translation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}