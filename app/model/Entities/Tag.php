<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Tag
 * @package App\Model\Entities
 * @ORM\Entity()
 */
class Tag
{
    use SmartObject;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $up = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $down = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return int
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * @return int
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * @param int $up
     */
    public function setUp($up)
    {
        $this->up = $up;
    }

    /**
     * @param int $down
     */
    public function setDown($down)
    {
        $this->down = $down;
    }

    function __construct($name)
    {
        $this->created = new \DateTime();

        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}