<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Seen
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_seen_uniq", columns={"user_id", "motion_picture_id"})})
 */
class Seen
{
    use SmartObject;
    use UserList;
}