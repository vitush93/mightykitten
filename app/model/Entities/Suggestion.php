<?php

namespace App\Model\Entities;


use App\Lib\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Suggestion
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 */
class Suggestion
{
    const SOURCE_MANUAL = 1,
        SOURCE_ALGORITHM = 2;

    use SmartObject;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="suggestions")
     */
    private $author;

    /**
     * @var MotionPicture
     * @ORM\ManyToOne(targetEntity="MotionPicture")
     * @ORM\JoinColumn(name="from_id", referencedColumnName="id")
     */
    private $from;

    /**
     * @var MotionPicture
     * @ORM\ManyToOne(targetEntity="MotionPicture")
     * @ORM\JoinColumn(name="to_id", referencedColumnName="id")
     */
    private $to;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $added;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $source = self::SOURCE_ALGORITHM;

    function __construct()
    {
        $this->added = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $author->addSuggestion($this);

        $this->author = $author;
    }

    /**
     * @return MotionPicture
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param MotionPicture $from
     */
    public function setFrom(MotionPicture $from)
    {
        $this->from = $from;
    }

    /**
     * @return MotionPicture
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param MotionPicture $to
     */
    public function setTo(MotionPicture $to)
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param int $source
     * @throws InvalidArgumentException
     */
    public function setSource($source)
    {
        if (!in_array($source, [self::SOURCE_ALGORITHM, self::SOURCE_MANUAL])) {
            throw new InvalidArgumentException("Source $source is not an allowed source parameter.");
        }

        $this->source = $source;
    }
}