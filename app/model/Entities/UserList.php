<?php

namespace App\Model\Entities;


trait UserList
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var MotionPicture
     * @ORM\ManyToOne(targetEntity="MotionPicture")
     */
    private $motionPicture;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * Constructor.
     * @param User $user
     * @param MotionPicture $motionPicture
     */
    function __construct(User $user, MotionPicture $motionPicture)
    {
        $this->created = new \DateTime();

        $this->setUser($user);
        $this->setMotionPicture($motionPicture);
    }

    /**
     * @return MotionPicture
     */
    public function getMotionPicture()
    {
        return $this->motionPicture;
    }

    /**
     * @param MotionPicture $motionPicture
     */
    public function setMotionPicture($motionPicture)
    {
        $this->motionPicture = $motionPicture;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}