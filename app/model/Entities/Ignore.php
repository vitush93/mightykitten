<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Ignore
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="user_ignore_uniq", columns={"user_id", "motion_picture_id"})})
 */
class Ignore
{
    use SmartObject;
    use UserList;
}