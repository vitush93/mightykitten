<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Setting
 * @package App\Model\Entities
 * @ORM\Entity()
 */
class Setting
{
    use SmartObject;

    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string", name="`key`")
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(type="string", name="`value`")
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(type="string", name="`comment`")
     */
    private $comment;

    /**
     * Setting constructor.
     * @param string $key
     */
    function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @param string $value
     */
    function set($value)
    {
        $this->value = $value;
    }

    function key()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    function get()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}