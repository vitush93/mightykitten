<?php

namespace App\Model\Entities;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Actor
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *          @ORM\Index(name="name_idx", columns={"name"})
 *     })
 */
class Actor
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * Actor constructor.
     * @param string $name
     */
    function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}