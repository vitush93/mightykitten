<?php

namespace App\Model\Entities;


use App\Lib\InvalidArgumentException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class Playlist
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *          @ORM\Index(name="type_idx", columns={"type"})
 *     })
 */
class Playlist
{
    use SmartObject;
    use Translatable;

    const TYPE_MOVIE = 'movie',
        TYPE_SERIES = 'series',
        TYPE_MIXED = 'mixed';

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PlaylistTranslation")
     * @ORM\JoinTable(name="translations_playlist",
     *                joinColumns={@ORM\JoinColumn(name="playlist_id", referencedColumnName="id")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="translation_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $translation;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="PlaylistItem", mappedBy="playlist", cascade={"persist", "remove"})
     */
    private $items;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="playlists_tags",
     *              joinColumns={@ORM\JoinColumn(name="playlist_id", referencedColumnName="id")},
     *              inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *     )
     */
    private $tags;

    /**
    * @var string
    * @ORM\Column(type="string", nullable=true)
    */
    private $thumbnail;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="playlists")
     */
    private $createdBy;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * Temporary lists are 'seasonal' eg. 'Before the sequel comes out'
     * If set, system will notify administrators that this list will be outdated on given date and required update or removal.
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $temporary;

    function __construct($type)
    {
        $this->translation = new ArrayCollection();
        $this->created = new \DateTime();
        $this->items = new ArrayCollection();
        $this->tags = new ArrayCollection();

        $this->setType($type);
    }

    function getThumbnail()
    {
        return $this->thumbnail;
    }

    function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return \DateTime
     */
    public function getTemporary()
    {
        return $this->temporary;
    }

    /**
     * @param \DateTime $temporary
     */
    public function setTemporary($temporary)
    {
        $this->temporary = $temporary;
    }

    /**
     * @param Tag $tag
     */
    function addTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws InvalidArgumentException
     */
    public function setType($type)
    {
        if (!in_array($type, [self::TYPE_MOVIE, self::TYPE_SERIES, self::TYPE_MIXED])) {
            throw new InvalidArgumentException("Type $type not recognized.");
        }

        $this->type = $type;
    }

    /**
     * @param MotionPicture $motionPicture
     * @return bool
     */
    function hasMotionPicture(MotionPicture $motionPicture)
    {
        $has = false;

        /** @var PlaylistItem $item */
        foreach ($this->items as $item) {
            if ($item->getMotionPicture() == $motionPicture) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    /**
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param PlaylistItem $item
     */
    function addItem(PlaylistItem $item)
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}