<?php

namespace App\Model\Entities;


use App\Lib\InvalidArgumentException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class MotionPicture
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *     @ORM\Index(name="imdb_idx", columns={"imdb_id"}),
 *     @ORM\Index(name="csfd_idx", columns={"csfd_id"})
 *     }
 * )
 */
class MotionPicture
{
    use Translatable;

    const TYPE_TV_SHOW = 'series',
        TYPE_MOVIE = 'movie';

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="MotionPictureTranslation")
     * @ORM\JoinTable(name="translations_picture",
     *                joinColumns={@ORM\JoinColumn(name="picture_id", referencedColumnName="id")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="translation_id", referencedColumnName="id", unique=true)})
     */
    private $translation;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Genre")
     * @ORM\JoinTable(name="picture_genres",
     *                joinColumns={@ORM\JoinColumn(name="picture_id", referencedColumnName="id")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="genre_id", referencedColumnName="id")})
     */
    private $genres;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Actor")
     * @ORM\JoinTable(name="picture_actors",
     *                joinColumns={@ORM\JoinColumn(name="picture_id", referencedColumnName="id")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="actor_id", referencedColumnName="id")})
     */
    private $actors;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="motion_pictures_tags",
     *              joinColumns={@ORM\JoinColumn(name="motion_picture_id", referencedColumnName="id")},
     *              inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *     )
     */
    private $tags;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $imdbId;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $csfdId;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $imdbRating;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $imdbVotes;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * e.g. 2011-
     *
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $year;

    /**
     * e.g. 10x56min
     *
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $runtime;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $language;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $poster;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $director;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $tomatoMeter;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $tomatoRating;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tomatoFresh;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tomatoRotten;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $tomatoUserMeter;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $tomatoUserRating;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $tomatoUrl;

    /**
     * @var int
     * @ORM\Column(type="string", nullable=true)
     */
    private $metascore;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $up = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $down = 0;

    /**
     * MotionPicture constructor.
     * @param $type
     */
    function __construct($type)
    {
        $this->created = new \DateTime();
        $this->tags = new ArrayCollection();
        $this->translation = new ArrayCollection();
        $this->genres = new ArrayCollection();
        $this->actors = new ArrayCollection();
        $this->setType($type);
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     */
    function addTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }
    }

    /**
     * @return int
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * @return int
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * @param int $up
     */
    public function setUp($up)
    {
        $this->up = $up;
    }

    /**
     * @param int $down
     */
    public function setDown($down)
    {
        $this->down = $down;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws InvalidArgumentException
     */
    public function setType($type)
    {
        if (!in_array($type, [self::TYPE_MOVIE, self::TYPE_TV_SHOW])) {
            throw new InvalidArgumentException("Type $type not in MotionPicture type list.");
        }

        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @return float
     */
    public function getTomatoMeter()
    {
        return $this->tomatoMeter;
    }

    /**
     * @param float $tomatoMeter
     */
    public function setTomatoMeter($tomatoMeter)
    {
        $this->tomatoMeter = $tomatoMeter;
    }

    /**
     * @return float
     */
    public function getTomatoRating()
    {
        return $this->tomatoRating;
    }

    /**
     * @param float $tomatoRating
     */
    public function setTomatoRating($tomatoRating)
    {
        $this->tomatoRating = $tomatoRating;
    }

    /**
     * @return int
     */
    public function getTomatoFresh()
    {
        return $this->tomatoFresh;
    }

    /**
     * @param int $tomatoFresh
     */
    public function setTomatoFresh($tomatoFresh)
    {
        $this->tomatoFresh = $tomatoFresh;
    }

    /**
     * @return int
     */
    public function getTomatoRotten()
    {
        return $this->tomatoRotten;
    }

    /**
     * @param int $tomatoRotten
     */
    public function setTomatoRotten($tomatoRotten)
    {
        $this->tomatoRotten = $tomatoRotten;
    }

    /**
     * @return float
     */
    public function getTomatoUserMeter()
    {
        return $this->tomatoUserMeter;
    }

    /**
     * @param float $tomatoUserMeter
     */
    public function setTomatoUserMeter($tomatoUserMeter)
    {
        $this->tomatoUserMeter = $tomatoUserMeter;
    }

    /**
     * @return float
     */
    public function getTomatoUserRating()
    {
        return $this->tomatoUserRating;
    }

    /**
     * @param float $tomatoUserRating
     */
    public function setTomatoUserRating($tomatoUserRating)
    {
        $this->tomatoUserRating = $tomatoUserRating;
    }

    /**
     * @return string
     */
    public function getTomatoUrl()
    {
        return $this->tomatoUrl;
    }

    /**
     * @param string $tomatoUrl
     */
    public function setTomatoUrl($tomatoUrl)
    {
        $this->tomatoUrl = $tomatoUrl;
    }

    /**
     * @param Genre $genre
     */
    function addGenre(Genre $genre)
    {
        if (!$this->genres->contains($genre)) {
            $this->genres->add($genre);
        }
    }

    /**
     * @return string
     */
    public function getImdbId()
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     */
    public function setImdbId($imdbId)
    {
        $this->imdbId = $imdbId;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCsfdId()
    {
        return $this->csfdId;
    }

    /**
     * @param string $csfdId
     */
    public function setCsfdId($csfdId)
    {
        $this->csfdId = $csfdId;
    }

    /**
     * @return float
     */
    public function getImdbRating()
    {
        return $this->imdbRating;
    }

    /**
     * @param float $imdbRating
     */
    public function setImdbRating($imdbRating)
    {
        $this->imdbRating = $imdbRating;
    }

    /**
     * @return int
     */
    public function getImdbVotes()
    {
        return $this->imdbVotes;
    }

    /**
     * @param int $imdbVotes
     */
    public function setImdbVotes($imdbVotes)
    {
        if ($imdbVotes) {
            // remove thousand separator
            $imdbVotes = str_replace(',', '', $imdbVotes);
        }

        $this->imdbVotes = $imdbVotes;
    }

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getRuntime()
    {
        return $this->runtime;
    }

    /**
     * @param string $runtime
     */
    public function setRuntime($runtime)
    {
        $this->runtime = $runtime;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * @param string $poster
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;
    }

    /**
     * @return string
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * @param string $director
     */
    public function setDirector($director)
    {
        $this->director = $director;
    }

    /**
     * @return ArrayCollection
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * @param Actor $actor
     */
    public function addActor(Actor $actor)
    {
        if (!$this->actors->contains($actor)) {
            $this->actors->add($actor);
        }
    }

    /**
     * @return int
     */
    public function getMetascore()
    {
        return $this->metascore;
    }

    /**
     * @param int $metascore
     */
    public function setMetascore($metascore)
    {
        $this->metascore = $metascore;
    }
}