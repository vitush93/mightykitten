<?php


namespace App\Model\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\InvalidArgumentException;
use Nette\Security\Passwords;
use Nette\SmartObject;
use Nette\Utils\Validators;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Model
 *
 * @ORM\Entity()
 */
class User
{
    use SmartObject;

    const   ROLE_USER = 'user',
        ROLE_MOD = 'mod',
        ROLE_ADMIN = 'admin';

    private static $ALLOWED_ROLES = [
        'user',
        'admin',
        'mod'
    ];

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * Suggestions added by this user.
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Suggestion", mappedBy="author")
     */
    private $suggestions;

    /**
     * Playlist created by this user.
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Playlist", mappedBy="createdBy")
     */
    private $playlists;

    /**
     * List of user's saved playlist (this user does not own them).
     *
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Playlist")
     * @ORM\JoinTable(name="saved_playlists",
     *          joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="playlist_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $savedPlaylists;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Favorite")
     * @ORM\JoinTable(name="users_favorites",
     *          joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="favorite_id", referencedColumnName="id", unique=true)}
     *     )
     *
     */
    private $favorites;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Seen")
     * @ORM\JoinTable(name="users_seen",
     *          joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="seen_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $seen;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="ToWatch")
     * @ORM\JoinTable(name="users_to_watch",
     *          joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="to_watch_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $toWatch;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="PlaylistItemRating", mappedBy="user")
     */
    private $playlistItemRatings;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="MotionPictureRating", mappedBy="user")
     */
    private $motionPictureRatings;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $role = self::ROLE_USER;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $facebookId;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $ban = false;

    /**
     * User constructor.
     * @param $email
     * @param $username
     * @param $password
     */
    function __construct($email, $username, $password)
    {
        $this->suggestions = new ArrayCollection();
        $this->playlists = new ArrayCollection();
        $this->playlistItemRatings = new ArrayCollection();
        $this->motionPictureRatings = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->toWatch = new ArrayCollection();
        $this->seen = new ArrayCollection();
        $this->savedPlaylists = new ArrayCollection();

        $this->setUsername($username);
        $this->setEmail($email);
        $this->setPassword($password);
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getSavedPlaylists()
    {
        return $this->savedPlaylists;
    }

    /**
     * @param Playlist $playlist
     */
    function addSavedPlaylist(Playlist $playlist)
    {
        $this->savedPlaylists->add($playlist);
    }

    /**
     * @return ArrayCollection
     */
    public function getFavorites()
    {
        return $this->favorites;
    }

    /**
     * @return ArrayCollection
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * @return ArrayCollection
     */
    public function getToWatch()
    {
        return $this->toWatch;
    }

    /**
     * @param MotionPicture $motionPicture
     */
    function addFavorite(MotionPicture $motionPicture)
    {
        $this->favorites->add($motionPicture);
    }

    /**
     * @param MotionPicture $motionPicture
     */
    function addSeen(MotionPicture $motionPicture)
    {
        $this->seen->add($motionPicture);
    }

    /**
     * @param MotionPicture $motionPicture
     */
    function addToWatch(MotionPicture $motionPicture)
    {
        $this->toWatch->add($motionPicture);
    }

    /**
     * @return boolean
     */
    public function isBanned()
    {
        return $this->ban;
    }

    public function unban()
    {
        $this->setBan(false);
    }

    public function ban()
    {
        $this->setBan(true);
    }

    /**
     * @param boolean $ban
     */
    public function setBan($ban)
    {
        $this->ban = $ban;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlaylistItemRatings()
    {
        return $this->playlistItemRatings;
    }

    /**
     * @param PlaylistItemRating $itemRating
     */
    function addPlaylistItemRating(PlaylistItemRating $itemRating)
    {
        $this->playlistItemRatings->add($itemRating);
    }

    /**
     * @return ArrayCollection
     */
    public function getMotionPictureRatings()
    {
        return $this->motionPictureRatings;
    }

    /**
     * @param MotionPictureRating $motionPictureRating
     */
    function addMotionPictureRating(MotionPictureRating $motionPictureRating)
    {
        $this->playlistItemRatings->add($motionPictureRating);
    }

    /**
     * @return ArrayCollection
     */
    public function getPlaylists()
    {
        return $this->playlists;
    }

    /**
     * @param Playlist $playlist
     */
    function addPlaylist(Playlist $playlist)
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists->add($playlist);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getSuggestions()
    {
        return $this->suggestions;
    }

    /**
     * @param Suggestion $suggestion
     */
    function addSuggestion(Suggestion $suggestion)
    {
        $this->suggestions->add($suggestion);
    }

    function setEmail($email)
    {
        if (!Validators::isEmail($email)) {
            throw new InvalidArgumentException();
        }

        $this->email = $email;
    }

    function setPassword($password)
    {
        $this->password = Passwords::hash($password);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    function __toString()
    {
        return "$this->email : $this->password";
    }

    /**
     * @param bool $pretty
     * @return string
     */
    public function getRole($pretty = false)
    {
        if ($pretty) {
            return ucfirst($this->role);
        }

        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        if (!in_array($role, self::$ALLOWED_ROLES)) {
            throw new InvalidArgumentException("Role $role is not within allowed roles.");
        }

        $this->role = $role;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return array
     */
    public static function getAllowedRoles()
    {
        return self::$ALLOWED_ROLES;
    }
}