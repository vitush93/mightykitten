<?php

namespace App\Model\Entities;


use App\Lib\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class PlaylistItemRating
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(
 *     indexes={
 *          @ORM\Index(name="rating_idx", columns={"rating"}),
 *          @ORM\Index(name="ip_idx", columns={"ip"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="rating_uniq", columns={"playlist_item_id", "user_id"})
 *     })
 */
class PlaylistItemRating
{
    use SmartObject;

    const RATING_UP = 1,
        RATING_DOWN = 2;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var PlaylistItem
     * @ORM\ManyToOne(targetEntity="PlaylistItem")
     */
    private $playlistItem;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="playlistItemRatings")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * PlaylistItemRating constructor.
     * @param $rating
     */
    function __construct($rating)
    {
        $this->created = new \DateTime();

        $this->setRating($rating);
    }

    function setUp()
    {
        $this->rating = self::RATING_UP;
    }

    function setDown()
    {
        $this->rating = self::RATING_DOWN;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @throws InvalidArgumentException
     */
    public function setRating($rating)
    {
        if (!in_array($rating, [self::RATING_DOWN, self::RATING_UP])) {
            throw new InvalidArgumentException("Rating $rating not recognozed.");
        }

        $this->rating = $rating;
    }

    /**
     * @return PlaylistItem
     */
    public function getPlaylistItem()
    {
        return $this->playlistItem;
    }

    /**
     * @param PlaylistItem $playlistItem
     */
    public function setPlaylistItem($playlistItem)
    {
        $this->playlistItem = $playlistItem;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $user->addPlaylistItemRating($this);

        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}