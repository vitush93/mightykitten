<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 *
 * Class PlaylistItem
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="playlist_motionpicture_uniq", columns={"playlist_id", "motion_picture_id"})})
 */
class PlaylistItem
{
    use SmartObject;

    const STATUS_APPROVED = 1,
        STATUS_PENDING = 2,
        STATUS_REJECTED = 3;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var Playlist
     * @ORM\ManyToOne(targetEntity="Playlist", inversedBy="items")
     */
    private $playlist;

    /**
     * @var MotionPicture
     * @ORM\ManyToOne(targetEntity="MotionPicture")
     */
    private $motionPicture;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $added;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $addedBy;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $up = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $down = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="`order`")
     */
    private $order = 0;

    /**
     * Denotes whether this playlist item is approved by administrator or not.
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_PENDING;

    function __construct()
    {
        $this->added = new \DateTime();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return Playlist
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * @param Playlist $playlist
     */
    public function setPlaylist($playlist)
    {
        $playlist->addItem($this);

        $this->playlist = $playlist;
    }

    /**
     * @return User
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * @param User $addedBy
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;
    }

    /**
     * @return int
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * @param int $up
     */
    public function setUp($up)
    {
        $this->up = $up;
    }

    /**
     * @return int
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * @param int $down
     */
    public function setDown($down)
    {
        $this->down = $down;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return MotionPicture
     */
    public function getMotionPicture()
    {
        return $this->motionPicture;
    }

    /**
     * @param MotionPicture $motionPicture
     */
    public function setMotionPicture($motionPicture)
    {
        $this->motionPicture = $motionPicture;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }
}