<?php

namespace App\Model\Entities;


use Kdyby\Doctrine\EntityManager;

trait Translatable
{
    private $newTranslations = [];
    private $cache = [];

    /**
     * @return bool
     */
    function hasNewTranslations()
    {
        return count($this->newTranslations) > 0;
    }

    /**
     * @param string $locale Locale lang code.
     * @return mixed
     */
    function translate($locale)
    {
        if (isset($this->cache[$locale])) return $this->cache[$locale];

        $translation = null;
        foreach ($this->translation as $t) {
            $this->cache[$t->getLocale()] = $t; // cache references to the Translation object

            if ($t->getLocale() == $locale) {
                $translation = $t;

                break;
            }
        }

        if ($translation) return $translation; else {
            $className = (new \ReflectionClass($this))->getName() . 'Translation';

            $newTranslation = new $className();
            $newTranslation->setLocale($locale);
            $this->translation->add($newTranslation);

            $this->newTranslations[] = &$newTranslation;

            return $newTranslation;
        }
    }

    /**
     * Persist all new translations.
     *
     * @param EntityManager $entityManager
     */
    function persistNewTranslations(EntityManager $entityManager)
    {
        foreach ($this->newTranslations as $tr) {
            $entityManager->persist($tr);
        }
    }
}