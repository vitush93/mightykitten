<?php

namespace App\Model\Entities;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class MotionPictureGenreTranslation
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *          @ORM\Index(name="name_idx", columns={"name"}),
 *          @ORM\Index(name="locale_idx", columns={"locale"}),
 *          @ORM\Index(name="name_locale_idx", columns={"name", "locale"})
 *     })
 */
class GenreTranslation
{
    use SmartObject;
    use Translation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}