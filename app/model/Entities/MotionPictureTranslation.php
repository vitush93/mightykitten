<?php

namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;

/**
 * Class MotionPictureTranslation
 * @package App\Model\Entities
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *          @ORM\Index(name="locale_idx", columns={"locale"})
 *     })
 */
class MotionPictureTranslation
{
    use SmartObject;
    use Translation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $plot;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPlot()
    {
        return $this->plot;
    }

    /**
     * @param string $plot
     */
    public function setPlot($plot)
    {
        $this->plot = $plot;
    }
}