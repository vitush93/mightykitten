<?php

namespace App\Model;


class Events
{
    const USER_ADD = 'user.add';
    const USER_REMOVE = 'user.remove';
    const SUGGESTION_ADD = 'suggestion.add';
    const MOTIONPICTURE_ADD = 'motionpicture.add';
    const MOVIE_ADD = 'movie.add';
    const TVSHOW_ADD = 'tvshow.add';
}