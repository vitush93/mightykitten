<?php

namespace App\Model\Suggestions;


use App\Model\Entities\Playlist;
use App\Model\FileManager;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class PlaylistManager
{
    use SmartObject;

    /** @var EntityManager */
    private $em;

    /**
     * PlaylistManager constructor.
     * @param EntityManager $entityManager
     */
    function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return \Kdyby\Doctrine\EntityRepository
     */
    private function repo()
    {
        return $this->em->getRepository(Playlist::class);
    }

    /**
     * @param $id
     * @return bool
     */
    function remove($id)
    {
        /** @var Playlist $playlist */
        $playlist = $this->repo()->find($id);

        if ($playlist) {
            $thumbnail = $playlist->getThumbnail();

            $this->em->remove($playlist);
            $this->em->flush();

            FileManager::delete($thumbnail);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $type
     * @return PlaylistBuilder
     */
    function createBuilder($type)
    {
        return new PlaylistBuilder($type);
    }
}