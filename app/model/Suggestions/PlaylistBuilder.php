<?php

namespace App\Model\Suggestions;


use App\Lib\ObjectWrapper;
use App\Model\Entities\Playlist;

class PlaylistBuilder
{
    use ObjectWrapper;

    function __construct($type)
    {
        $this->object = new Playlist($type);
    }

    /**
     * @param $name
     * @param $locale
     * @return $this
     */
    function setName($name, $locale)
    {
        $this->object->translate($locale)->setName($name);

        return $this;
    }

    /**
     * @param $desc
     * @param $locale
     * @return $this
     */
    function setDescription($desc, $locale)
    {
        $this->object->translate($locale)->setDescription($desc);

        return $this;
    }

    /**
     * @return Playlist
     */
    function getPlaylist()
    {
        return $this->object;
    }
}