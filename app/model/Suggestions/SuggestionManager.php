<?php

namespace App\Model\Suggestions;


use App\Lib\InvalidOperationException;
use App\Model\Entities\MotionPicture;
use App\Model\Entities\Suggestion;
use App\Model\Entities\User;
use App\Model\MotionPicture\MotionPictureManager;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class SuggestionManager
{
    use SmartObject;

    /** @var EntityManager */
    private $em;

    /** @var MotionPictureManager */
    private $motionPictureManager;

    /**
     * SuggestionManager constructor.
     * @param EntityManager $entityManager
     * @param MotionPictureManager $motionPictureManager
     */
    function __construct(EntityManager $entityManager, MotionPictureManager $motionPictureManager)
    {
        $this->em = $entityManager;
        $this->motionPictureManager = $motionPictureManager;
    }

    /**
     * @param $id
     * @return bool
     */
    function remove($id)
    {
        $sug = $this->em->find(Suggestion::class, $id);
        if ($sug) {
            $this->em->remove($sug);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param MotionPicture $from
     * @param MotionPicture $to
     * @return mixed
     */
    function findByTuple(MotionPicture $from, MotionPicture $to)
    {
        return $this->em->createQueryBuilder()
            ->select('s')
            ->from(Suggestion::class, 's')
            ->join('s.from', 'f')
            ->join('s.to', 't')
            ->where('(f.imdbId = :from AND t.imdbId = :to) OR (f.imdbId = :to AND t.imdbId = :from)')
            ->setParameter('from', $from->getImdbId())
            ->setParameter('to', $to->getImdbId())
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param $fromID
     * @param $toID
     * @return Suggestion
     */
    function addByImdbID(User $user, $fromID, $toID)
    {
        $from = $this->motionPictureManager->findOneOrCreate(['imdbId' => $fromID], $fromID);
        $to = $this->motionPictureManager->findOneOrCreate(['imdbId' => $toID], $toID);

        return $this->add($user, $from, $to);
    }

    /**
     * @param User $user
     * @param MotionPicture $from
     * @param MotionPicture $to
     * @return Suggestion
     * @throws InvalidOperationException
     */
    function add(User $user, MotionPicture $from, MotionPicture $to)
    {
        $existing = $this->findByTuple($from, $to);
        if ($existing) {
            throw new InvalidOperationException("Suggestion already exists.");
        }

        $suggestion = new Suggestion();
        $suggestion->setAuthor($user);
        $suggestion->setFrom($from);
        $suggestion->setTo($to);

        $this->em->persist($suggestion);

        return $suggestion;
    }
}