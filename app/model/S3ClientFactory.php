<?php

namespace App\Model;


use Aws\S3\S3Client;

class S3ClientFactory
{
    private $version;
    private $region;
    private $key;
    private $secret;

    /**
     * S3ClientFactory constructor.
     * @param $version
     * @param $region
     * @param $key
     * @param $secret
     */
    function __construct($region, $key, $secret, $version = 'latest')
    {
        $this->version = $version;
        $this->region = $region;
        $this->key = $key;
        $this->secret = $secret;
    }

    function create()
    {
        $s3 = new S3Client([
            'version' => $this->version,
            'region' => $this->region,
            'credentials' => [
                'key' => $this->key,
                'secret' => $this->secret
            ]
        ]);

        return $s3;
    }
}