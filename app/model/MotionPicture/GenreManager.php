<?php

namespace App\Model\MotionPicture;


use App\Model\Entities\Genre;
use App\Model\Entities\GenreTranslation;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class GenreManager
{
    use SmartObject;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * GenreManager constructor.
     * @param EntityManager $entityManager
     */
    function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $name
     * @param $locale
     * @return Genre|null
     */
    function find($name, $locale)
    {
        /** @var Genre|null $genre */
        $genre = $this->em->createQueryBuilder()
            ->select('g')
            ->from(Genre::class, 'g')
            ->join('g.translation', 't')
            ->where("t.name = :name")
            ->andWhere("t.locale = :locale")
            ->setParameter('name', $name)
            ->setParameter('locale', $locale)
            ->getQuery()->getOneOrNullResult();

        return $genre;
    }

    /**
     * @param $name
     * @param $locale
     * @return Genre
     */
    static function create($name, $locale)
    {
        $genre = new Genre();
        $genre->translate($locale)->setName($name);

        return $genre;
    }

    /**
     * @param Genre $genre
     * @param $name
     * @param $locale
     */
    function addTranslation(Genre $genre, $name, $locale)
    {
        $genre->translate($locale)->setName($name);
        if ($genre->hasNewTranslations()) {
            $genre->persistNewTranslations($this->em);
        }
    }

    /**
     * @param $name
     * @param $locale
     * @return Genre
     */
    function add($name, $locale)
    {
        $genre = self::create($name, $locale);
        $genre->persistNewTranslations($this->em);

        $this->em->persist($genre);

        return $genre;
    }
}