<?php

namespace App\Model\MotionPicture;


use App\Model\Entities\Tag;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class MetaInfo
{
    use SmartObject;

    private $em;

    function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $name
     * @return Tag
     */
    function addTag($name)
    {
        $tag = new Tag($name);
        $this->em->persist($tag);

        return $tag;
    }
}