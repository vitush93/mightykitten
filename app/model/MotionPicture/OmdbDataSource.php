<?php

namespace App\Model\MotionPicture;


use App\Lib\InvalidArgumentException;
use App\Lib\InvalidOperationException;
use App\Model\Entities\Actor;
use App\Model\Entities\Genre;
use App\Model\Entities\MotionPicture;
use App\Model\FileManager;
use GuzzleHttp\Client;
use Kdyby\Doctrine\EntityManager;
use Nette\Database\Context;
use Nette\NotImplementedException;
use Nette\Utils\Json;

class OmdbDataSource implements DataSource
{
    const BASE_API_URL = 'http://www.omdbapi.com/';

    /** @var GenreManager */
    private $genreManager;

    /** @var Context */
    private $csfdDatabase;

    /** @var EntityManager */
    private $em;

    /** @var FileManager */
    private $fileManager;

    private $genresToPersist = [];

    private $actorsToPersist = [];

    /** @var MotionPictureBuilder|MotionPicture */
    private $builder;

    /**
     * OmdbDataSource constructor.
     * @param GenreManager $genreManager
     * @param Context $context
     * @param EntityManager $entityManager
     * @param FileManager $fileManager
     */
    function __construct(GenreManager $genreManager, Context $context, EntityManager $entityManager, FileManager $fileManager)
    {
        $this->genreManager = $genreManager;
        $this->csfdDatabase = $context;
        $this->em = $entityManager;
        $this->fileManager = $fileManager;
    }

    /**
     * @return MotionPicture|MotionPictureBuilder
     * @throws InvalidOperationException
     */
    function getBuilder()
    {
        if (!$this->builder) throw new InvalidOperationException("Builed not instantiated.");

        return $this->builder;
    }

    /**
     * @param string $id Service-specific unique motion picture identifier.
     * @return $this
     * @throws InvalidArgumentException
     */
    function fetch($id)
    {
        $client = new Client();
        $response = $client->request('GET', self::BASE_API_URL, [
            'headers' => ['Accept-Encoding' => 'gzip'],
            'query' => [
                'i' => $id,
                'tomatoes' => 'true',
                'plot' => 'full'
            ]
        ]);

        $data = Json::decode($response->getBody()->getContents());
        if ($data->Response == 'False') {
            throw new InvalidArgumentException("Item with Omdb ID $id not found.");
        }

        return $this->createFromRawData($data);
    }

    /**
     * @param string $query Search service for motion picture by query string.
     * @return mixed
     */
    function search($query)
    {
        throw new NotImplementedException();
    }

    /**
     * @param $data
     * @return $this
     */
    private function createFromRawData($data)
    {
        /** @var MotionPictureBuilder|MotionPicture $mbp */
        $mbp = new MotionPictureBuilder($data->Type);

        // normalize 'N/A' values
        foreach ($data as $key => $value) {
            if ($value == 'N/A') {
                $data->{$key} = null;
            }
        }

        // base data
        $mbp->setTitle($data->Title, 'en');
        $mbp->setPlot($data->Plot, 'en');
        $mbp->setYear($data->Year);
        $mbp->setImdbId($data->imdbID);
        $mbp->setDirector($data->Director);
        $mbp->setCountry($data->Country);
        $mbp->setImdbRating($data->imdbRating);
        $mbp->setImdbVotes($data->imdbVotes);
        $mbp->setCsfdId(
            $this->findCsfdId($data->imdbID)
        );
        $mbp->setLanguage($data->Language);
        $mbp->setRuntime($data->Runtime);
        $mbp->setTomatoFresh($data->tomatoFresh);
        $mbp->setTomatoMeter($data->tomatoMeter);
        $mbp->setTomatoRating($data->tomatoRating);
        $mbp->setTomatoRotten($data->tomatoRotten);
        $mbp->setTomatoUrl($data->tomatoURL);
        $mbp->setTomatoUserMeter($data->tomatoUserMeter);
        $mbp->setTomatoUserRating($data->tomatoUserRating);

        // actors
        $actors = explode(',', $data->Actors);
        $actors = array_map('trim', $actors);
        $this->processActors($mbp, $actors);

        // genres
        $genres = explode(',', $data->Genre);
        $genres = array_map('trim', $genres);
        $this->processGenres($mbp, $genres);

        if ($data->Poster) {
            // fetch poster
            $mbp->setPoster(
                $this->fileManager->fetch($data->Poster)
            );
        }

        $this->builder = $mbp;
        return $this;
    }

    /**
     * @param $imdbId
     * @return string
     */
    private function findCsfdId($imdbId)
    {
        $csfd = $this->csfdDatabase->table('raw_data')
            ->where('imdb_id', $imdbId)
            ->fetch();
        if ($csfd) {
            return $csfd->csfd_id;
        } else {
            return '';
        }
    }

    /**
     * @param MotionPictureBuilder|MotionPicture $motionPictureBuilder
     * @param array $actors
     */
    private function processActors(MotionPictureBuilder $motionPictureBuilder, array $actors)
    {
        foreach ($actors as $name) {
            $filtered = array_filter($this->actorsToPersist, function ($item) use ($name) {
                return $item->getName() == $name;
            });

            if (count($filtered) > 0) {
                $actor = array_pop($filtered);
            } else {
                $actor = $this->em->getRepository(Actor::class)
                    ->findOneBy(['name' => $name]);
            }

            if (!$actor) {
                $actor = new Actor($name);
                $this->actorsToPersist[] = $actor;
            } else {
                $motionPictureBuilder->addActor($actor);
            }
        }
    }

    /**
     * @param MotionPictureBuilder|MotionPicture $motionPictureBuilder
     * @param array $genres
     */
    private function processGenres(MotionPictureBuilder $motionPictureBuilder, array $genres)
    {
        foreach ($genres as $name) {
            $filtered = array_filter($this->genresToPersist, function ($item) use ($name) {
                return $item->translate('en')->getName() == $name;
            });

            if (count($filtered) > 0) {
                $genre = array_pop($filtered);
            } else {
                $genre = $this->genreManager->find($name, 'en');
            }

            if (!$genre) {
                $genre = GenreManager::create($name, 'en');
                $this->genresToPersist[] = $genre;
            } else {
                $motionPictureBuilder->addGenre($genre);
            }
        }
    }

    /**
     * @return mixed
     * @throws InvalidOperationException
     */
    function persist()
    {
        if (!$this->builder) throw new InvalidOperationException("Nothing to persist: Builder is not instantiated.");

        $motionPicture = $this->builder->getMotionPicture();

        /** @var Genre $genre */
        foreach ($this->genresToPersist as $genre) {
            if ($genre->hasNewTranslations()) {
                $genre->persistNewTranslations($this->em);
            }

            $this->em->persist($genre);
            $motionPicture->addGenre($genre);
        }

        /** @var Actor $actor */
        foreach ($this->actorsToPersist as $actor) {
            $this->em->persist($actor);
            $motionPicture->addActor($actor);
        }

        if ($motionPicture->hasNewTranslations()) {
            $motionPicture->persistNewTranslations($this->em);
        }

        $this->em->persist($motionPicture);

        return $motionPicture;
    }
}