<?php

namespace App\Model\MotionPicture;


use App\Model\Entities\MotionPicture;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class MotionPictureManager
{
    use SmartObject;

    /** @var EntityManager */
    private $em;

    /** @var OmdbDataSource */
    private $omdbDataSource;

    /**
     * MotionPictureManager constructor.
     * @param EntityManager $entityManager
     * @param OmdbDataSource $omdbDataSource
     */
    function __construct(EntityManager $entityManager, OmdbDataSource $omdbDataSource)
    {
        $this->em = $entityManager;
        $this->omdbDataSource = $omdbDataSource;
    }

    /**
     * @return \Kdyby\Doctrine\EntityRepository
     */
    private function repo()
    {
        return $this->em->getRepository(MotionPicture::class);
    }

    /**
     * @param $imdbID
     * @return mixed|null|object
     */
    function findByImdbID($imdbID)
    {
        return $this->repo()->findOneBy(['imdb_id' => $imdbID]);
    }

    /**
     * @param $csfdID
     * @return mixed|null|object
     */
    function findByCsfdID($csfdID)
    {
        return $this->repo()->findOneBy(['csfd_id' => $csfdID]);
    }

    /**
     * @param $imdbID
     * @return mixed
     */
    function createByImdbID($imdbID)
    {
        return $this->omdbDataSource->fetch($imdbID)
            ->persist();
    }

    /**
     * @param array $crit
     * @param $imdbID
     * @return mixed|null|object
     */
    function findOneOrCreate(array $crit, $imdbID)
    {
        $mp = $this->repo()->findOneBy($crit);
        if(!$mp) {
            $mp = $this->createByImdbID($imdbID);
        }

        return $mp;
    }
}