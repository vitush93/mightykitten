<?php

namespace App\Model\MotionPicture;


use App\Lib\ObjectWrapper;
use App\Model\Entities\MotionPicture;
use App\Model\Entities\Genre;

class MotionPictureBuilder
{
    use ObjectWrapper;

    /**
     * MotionPictureBuilder constructor.
     * @param string $type TV Show ('series') or a Movie ('movie').
     */
    function __construct($type)
    {
        $this->object = new MotionPicture($type);
    }

    /**
     * @param string $title
     * @param string $locale
     */
    function setTitle($title, $locale)
    {
        $this->object->translate($locale)->setTitle($title);
    }

    /**
     * @param string $plot
     * @param string $locale
     */
    function setPlot($plot, $locale)
    {
        $this->object->translate($locale)->setPlot($plot);
    }

    /**
     * @param Genre $genre
     */
    function addGenre(Genre $genre)
    {
        $this->object->addGenre($genre);
    }

    /**
     * @return MotionPicture
     */
    function getMotionPicture()
    {
        return $this->object;
    }
}