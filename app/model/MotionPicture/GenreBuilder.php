<?php

namespace App\Model\MotionPicture;


use App\Lib\ObjectWrapper;
use App\Model\Entities\Genre;

class GenreBuilder
{
    use ObjectWrapper;

    function __construct()
    {
        $this->object = new Genre();
    }

    /**
     * @param $name
     * @param $locale
     */
    function setName($name, $locale)
    {
        $this->object->translate($locale)->setName($name);
    }
}