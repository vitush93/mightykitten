<?php

namespace App\Model\MotionPicture;


use Kdyby\Doctrine\EntityManager;

interface DataSource
{
    /**
     * @param string $id Service-specific unique motion picture identifier.
     * @return mixed
     */
    function fetch($id);

    /**
     * @param string $query Search service for motion picture by query string.
     * @return mixed
     */
    function search($query);

    /**
     * @return mixed
     */
    function persist();
}