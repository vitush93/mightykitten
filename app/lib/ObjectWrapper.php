<?php

namespace App\Lib;


trait ObjectWrapper
{
    /**
     * Actual instance.
     */
    private $object;

    /**
     * Delegates methods to the actual object instance.
     *
     * @param $name
     * @param $arguments
     * @return mixed|$this
     */
    function __call($name, $arguments)
    {
        $result = call_user_func_array([$this->object, $name], $arguments);

        return $result ? $result : $this;
    }
}