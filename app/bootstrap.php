<?php
$debug_ips = require 'debug_ips.php';

require __DIR__ . '/lib/functions.php';
require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

// TODO remove when ready for production
if (php_sapi_name() !== 'cli') {
    $configurator->setDebugMode(array_keys($debug_ips)); // enable for your remote IP
}
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
