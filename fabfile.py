import os
import sys
from contextlib import contextmanager
from os.path import abspath, join, pardir

from fabric.api import env, run
from fabric.context_managers import lcd, cd, prefix, settings
from fabric.decorators import task, hosts
from fabric.operations import sudo
from fabric.tasks import execute

sys.path.append(abspath(join(os.path.dirname(__file__), pardir)))

PATHS = {
    'root': '/var/www/html',
    'www': '/var/www/html/www',
    'temp': '/var/www/html/temp'
}

HOSTS = {
    "production": "mightykitten.net"
}

# configure SSH
env.key_filename = "~/.ssh/id_rsa"
env.forward_agent = True
env.user = "vitush"


@task()
def deploy():
    with cd(PATHS['root']):

        # drop temp
        sudo('rm -rf temp/*')

        # git pull
        run('git pull')

        # restart PHP-FPM
        sudo('service php7.0-fpm reload')